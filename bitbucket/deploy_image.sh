#!/usr/bin/env bash
#
# deploy this bitbucket build to an environment. Assumes an image with same commit ID as current build has been stored in
# ECR (Amazon docker repo)

USAGE="deploy_image.sh env [ service ]"

if [ $# -lt 1 ]
then
    echo "Missing env parameter"
    echo "${USAGE}"
    exit 1
fi

GENVIS_ENV=$1

# TODO - how can we centralise this - or rename to env
CLUSTER_NAME="Good2Go_Backend"
# need to fix below - switch to new ECR repo.

source bitbucket/env_vars

# LATEST_TAG=current.${GENVIS_LOCATION}.${GENVIS_ENV}
LATEST_TAG=current.${GENVIS_ENV}

setupEnv

if [ $# -gt 1 ]
then
    SERVICE_NAME=$2
fi

echo "deploying service name ${SERVICE_NAME}"

loginToECR

set -ex

# move the current tag in docker repo for this environment
docker pull $ECR_REPO:$UNIQUE_TAG
docker tag $ECR_REPO:$UNIQUE_TAG $ECR_REPO:$LATEST_TAG
docker push $ECR_REPO:$LATEST_TAG

deployService

#AWS_ACCESS_KEY_ID=${DEPLOY_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${DEPLOY_SECRET_ACCESS_KEY} aws ecs update-service --region ${AWS_DEPLOY_REGION} --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME}  --force-new-deployment
#
#echo "Waiting for services to stabilize"
## Services take >60 sec to stabilize
#sleep 15
#
## It will poll every 15 seconds until a successful state has been reached.
## This will exit with a return code of 255 after 40 failed checks.
#AWS_ACCESS_KEY_ID=${DEPLOY_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${DEPLOY_SECRET_ACCESS_KEY} aws ecs wait services-stable --region ${AWS_DEPLOY_REGION} --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME}
#
#RETVAL=$?
#
#if [[ $RETVAL -eq 0 ]]; then
#   echo "New ${SERVICE_NAME} new is up and running."
#else
#   echo "Problem - timed out waiting for ${SERVICE_NAME} to stabilize."
#   exit 1;
#fi
