FROM node:10

ARG NPM_TOKEN
ENV NPM_TOKEN=$NPM_TOKEN

RUN echo $NPM_TOKEN

WORKDIR /usr/app

COPY .npmrc /usr/app

COPY package.json /usr/app 

RUN npm i 
RUN npm i -g mocha knex

COPY . /usr/app

EXPOSE 8082

CMD ["node", "index.js"]