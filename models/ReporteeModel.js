const log = require('@genvis/genvis-log').childLogger('routes:models:ReporteeModel')
const Promise = require('bluebird')

const REPORTEES = [
  {id: 1, user_id: 1, first_name: "first_name", last_name: "last_name"},
  {id: 2, user_id: 2, first_name: "first_name", last_name: "last_name"},
  {id: 3, user_id: 3, first_name: "first_name", last_name: "last_name"}
];

class ReporteeModel {

  constructor() {}

  
  findByUserID ({ user_id }) {
    return  Promise.resolve( REPORTEES.find( reportee => reportee.user_id === user_id) );
  }


  getReportees ({ }) {
    return  Promise.resolve( REPORTEES );
  }


}

module.exports = ReporteeModel;