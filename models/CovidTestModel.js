const moment = require('moment');
const CLINICS = [
  {id: 1, name: 'Clinic 1'},
  {id: 2, name: 'Clinic 2'},
  {id: 3, name: 'Clinic 3'},
];
const COVID_TESTS = [];
class CovidTestModel {
  constructor() {}

  async create({ record }) {

    const createdCovidTest = { ...record, id: COVID_TESTS.length };
    COVID_TESTS.push( createdCovidTest )
    return Promise.resolve( createdCovidTest );

  }
  
  async getCovidTests({ reportee }) {
    return  Promise.resolve( COVID_TESTS.filter( test => test.reportee_id === reportee.id) );
  }

  async find({ id }) {
    return  Promise.resolve( COVID_TESTS.find( test => test.id === id) );
  }

  async update({ id, positive_test, user_id }) {
      
    let test = COVID_TESTS.find( test => test.id === id)
    test.updated_at = moment();
    test.updated_by = user_id;
    test.positive_test = positive_test;

    return Promise.resolve( test );
  }

  async getClinics ({ }) {
    return Promise.resolve( CLINICS );
  }


}

module.exports = CovidTestModel;