const moment = require("moment");
const log = require('@genvis/genvis-log').childLogger('service:ReporteeService');
const { verifyUserOwnsReportee } = require('../controllers/helpers')

const ReporteeService = require("./ServiceUtils").dbService({
  
  name: "ReporteeService",

  props: {
    ReporteeModel: require("../models/ReporteeModel"),
  },

  methods: {
  
    async getReportees({ reportee }) {
      const reporteeModel = new this.ReporteeModel();
      return await reporteeModel.getReportees({ });
    },    
    
    
  }
});

module.exports = ReporteeService;