const assert = require("assert");
const stampit = require("stampit");


const HasConfig = stampit({
    props: {
        config: require("../cfg/config")
    },
    init({ config }) {
        this.config = config || this.config;
    }
});

const HasLog = stampit({
    props: {
        log: null
    },
    init(_, { stamp }) {
        if (this.log) return; // logger already exists
        assert(stamp.name !== "Stamp", "Couldn't initialize logger. No name was given.");
        this.log = require("@genvis/genvis-log").childLogger(`service:${stamp.name}`);
    }
});

const HasKnex = stampit({
    props: {
        knex: null,
        knexReader: null,
    },
    init(_, { stamp }) {
        if (this.knex) return; // knex already exists
        this.knex = require("@genvis/genvis-knex");
    }
});

/**
 * Adds MyService.getInstance() singleton getter.
 */
const Singleton = stampit(HasConfig, {
    statics: {
        instance: null,

        /**
         * Returns the singleton instance of the service
         */
        getInstance(...args) {
            this.instance = this.instance || this.create(...args);
            return this.instance;
        }
    }
});


/** This is the base for your services.
 * @function
 */
const Service = stampit(HasConfig, HasLog, Singleton, {
    statics: {
        service(opts) {
            const { name } = opts;
            assert(name, "Service name is mandatory");
            // assert(knex, "knex connection is mandatory");
            return this.compose(opts);
        }
    }
});

const DBService = stampit(HasConfig, HasLog, HasKnex, Singleton, {
    statics: {
        service(opts) {
            const { name } = opts;
            assert(name, "Service name is mandatory");
            return this.compose(opts);
        }
    }
});
/**
 * Utility function to pre-setup your service.
 * @param name {String} Your service name. Required. Used in logger.
 * @return {Function}
 */
const service = (...args) => Service.service(...args);

const dbService = (...args) => DBService.service(...args);


module.exports = { Singleton, service, dbService };