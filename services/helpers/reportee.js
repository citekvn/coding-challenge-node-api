const moment = require("moment")
const assert = require('assert')

const log = require('@genvis/genvis-log').childLogger('services:helpers:reportee')

module.exports = {
  getQuarantineStatus: function ({ reportee, quarantine_start, quarantine_end }) {

    log.debug({
      reportee,
      quarantine_start,
      quarantine_end
    }, 'Checking quarantine status')
  
    const now = moment()
    let quarantine_status = "N/A";
    
    if (!quarantine_start || !quarantine_end) {
      log.warn({ 
        reportee, 
        quarantine_start,
        quarantine_end
      }, 'Reportee does not have a valid quarantine period.')
      return 'N/A'
    } 
    
    if (!moment.isMoment(quarantine_start)) {
      quarantine_start = moment(quarantine_start)
    }
    if (!moment.isMoment(quarantine_end)) {
      quarantine_end = moment(quarantine_end)
    }
  
    if (now.isBefore(quarantine_start)) {
      quarantine_status = "Not started"
    } else if (now.isAfter(quarantine_start) && now.isBefore(quarantine_end)) {
      quarantine_status = "In quarantine"
    } else if (now.isAfter(quarantine_end)) {
      quarantine_status = "Completed"
    }
  
    return quarantine_status
  },
  calculateQuarantineDays: function ({ quarantine_start, quarantine_end }) {
    assert(quarantine_start && quarantine_end, 'Must supply valid quarantine start & end dates to calculate quarantine days');
  
    if (!moment.isMoment(quarantine_start)) {
      quarantine_start = moment(quarantine_start)
    }
    if (!moment.isMoment(quarantine_end)) {
      quarantine_end = moment(quarantine_end)
    }
  
    return quarantine_end.diff(quarantine_start, 'days')
  }
}



