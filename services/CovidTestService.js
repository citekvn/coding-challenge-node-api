const moment = require("moment");
const log = require('@genvis/genvis-log').childLogger('service:CovidTestService');
const { verifyUserOwnsReportee } = require('../controllers/helpers')

const CovidTestService = require("./ServiceUtils").dbService({
  name: "CovidTestService",

  props: {
    CovidTestModel: require("../models/CovidTestModel"),
    ReporteeModel: require("../models/ReporteeModel"),
  },

  methods: {
  
    async create({reportee_id, positive_test, clinic_id, user}) {

      const covidTestModel = new this.CovidTestModel();

      let reportee;
      const user_id = user.id;
      if (!reportee_id) {
        let reporteeModel = new this.ReporteeModel();
        reportee = await reporteeModel.findByUserID({ user_id });
        if (!reportee) {
          throw new Error(`No reportee associated with user ${user_id}`);
        }
      }

      return await covidTestModel.create({
        record: {
          reportee_id: reportee_id || reportee.id, 
          positive_test, 
          clinic_id,
          created_by: user_id,
          created_at: moment(),
          updated_at: moment(),
        }        
      });

    },

    async update({id, positive_test, user, req}) {

      const covidTestModel = new this.CovidTestModel();
      const user_id = user.id;

      const covid_test = await covidTestModel.find({ id })
      if (!covid_test) {
        log.warn({user_id, covid_test_id: id }, "COVID test record does not exist");
        throw new Error(`COVID test record with that id does not exist. (id=${id})`)
      }

      verifyUserOwnsReportee({
        reportee_id: covid_test.reportee_id,
        reportee: req.reportee,
        user,
        message: 'Could not update COVID test record',
        route: 'Update COVID test',
      })
      

      return await covidTestModel.update({ id, positive_test, user_id: user.id });

    },

    async getClinics() {
      const covidTestModel = new this.CovidTestModel();
      return await covidTestModel.getClinics({});
    },

    async getCovidTests({ reportee }) {
      const covidTestModel = new this.CovidTestModel();
      return await covidTestModel.getCovidTests({ reportee });
    },    
    
    
  }
});

module.exports = CovidTestService;
