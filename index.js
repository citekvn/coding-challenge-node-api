const { server, app, log, PORT } = require('./server')

try {
  server.listen(PORT, () => {
    log.info(`** Server started on PORT ${PORT} **`)
  })
} catch (err) {
  log.err({err}, 'Could not start express application.')
}