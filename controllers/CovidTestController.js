const log = require('@genvis/genvis-log').childLogger('controllers:CovidTestController');
const _ = require('lodash')

const {
  verifyUserOwnsReportee,
  successResponse,
  errorResponse,
  validateInteger,
  fieldRequired,  
  validateBoolean,
} = require('./helpers')

const {
  UnauthorisedError    
} = require('../utils/errors');

let CovidTestService = null;
class CovidTestController {

  constructor({ CovidTestSVC }) {
    
    CovidTestService = CovidTestSVC ? CovidTestSVC : require('../services/CovidTestService');
    
  }

  async getCovidTests (req, res) {

    const { user } = _.get(req, "tokenSession.data", {});
    const user_id = user.id;


    try {
      

      if( !req.reportee ){
        throw new UnauthorisedError();
      }

      let covidTestService = new CovidTestService();
      let covid_tests = await covidTestService.getCovidTests({ reportee: req.reportee });

      return successResponse({
        res, data: covid_tests, message: 'Succesfully get covid test records.'
      });
      
    } catch(err) {

      if (err.name === 'UnauthorisedError') {
        log.error({ user_id, err }, 'Unauthorised to create COVID test')
        res.status(401);
        return res.send({
          message: "Unauthorised",
          status: "Error"
        });
      }

      return errorResponse({
        res, user_id, log, err, message: 'Could not create covid test record.'
      })
    }


  }

  async create (req, res) {

    const { reportee_id, positive_test, clinic_id }  = req.body;
    const { user }        = _.get(req, "tokenSession.data", {});
    const user_id = user.id;

    try {

      log.info({ body: req.body, user_id }, 'Creating covid test result');

      // fieldRequired({ value: reportee_id, name: 'reportee_id'});
      // fieldRequired({ value: positive_test, name: 'positive_test'});
      // fieldRequired({ value: clinic_id, name: 'clinic_id'});

      validateInteger({ number: reportee_id, errMessage: 'reportee_id value is not integer'});
      validateInteger({ number: clinic_id, errMessage: 'clinic_id value is not integer'});
      validateBoolean({ value: positive_test, errMessage: 'positive_test value is not boolean'});



      verifyUserOwnsReportee({
        reportee_id: reportee_id,
        reportee: req.reportee,
        user,
        message: 'Could not create COVID test',
        route: 'Create COVID test'
      });

      let covidTestService = new CovidTestService();
      let created_record = await covidTestService.create({ reportee_id, positive_test, clinic_id, user, req });

      return successResponse({
        res, data: created_record, message: 'Succesfully created covid test record.'
      });
      
    } catch(err) {

      if (err.name === 'UnauthorisedError') {
        log.error({ user_id, err }, 'Unauthorised to create COVID test')
        res.status(401);
        return res.send({
          message: "Unauthorised",
          status: "Error"
        });
      }

      return errorResponse({
        res, user_id, log, err, message: 'Could not create covid test record.'
      })
    }
  }

  async update (req, res) {

    let { id, positive_test } = req.body;
    let { user } = req.tokenSession.data;

    try {

      log.info({ body: req.body, user_id: user.id }, 'Updating COVID test')

      validateInteger({ number: id, errMessage: 'id value is not integer'});
      validateBoolean({ value: positive_test, errMessage: 'positive_test value is not boolean'});


      verifyUserOwnsReportee({
        reportee_id: id,
        reportee: req.reportee,
        user,
        message: 'Could not create COVID test',
        route: 'Update COVID test'
      });

      let covidTestService = new CovidTestService();
      let updated_record = await covidTestService.update({ id, positive_test, user, req })

      return successResponse({
        res, data: updated_record, message: 'Successfully updated COVID test record.'
      })
    } catch(err) {

      if (err.name === 'UnauthorisedError') {
        res.status(401)
        return res.send({
          status: "Error", 
          message: "Unauthorised"
        })
      }

      return errorResponse({
        res, log, err, message: 'Could not update COVID test record.'
      })
    }
  }

  async getClinics (req, res) {
    try {
      let covidTestService = new CovidTestService();
      let covid_clinics = await covidTestService.getClinics();
      return successResponse({
        res, 
        data: covid_clinics,
        message: 'Fetched COVID clinics.'
      })
    } catch(err) {
      return errorResponse({
        res, 
        log, 
        err, 
        message: 'Could not fetch COVID clinics'
      })
    }
  }

}

module.exports = CovidTestController;