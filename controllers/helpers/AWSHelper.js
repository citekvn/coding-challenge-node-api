const AWS = require("aws-sdk")

let s3Params 

AWS.config = new AWS.Config()
s3Params = {
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_KEY_ID,
  signatureVersion: 'v4',
  region: process.env.AWS_REGION || "ap-southeast-2"
}

const s3 = new AWS.S3(s3Params)


const getObjectUrl = (bucket, key) => {
  let params = { Bucket: bucket, Key: key, Expires: 10000}
  try {
    return s3.getSignedUrl('getObject', params)
  } catch(e) {    
    throw e;
  }
}


const getObjectUploadUrl = (bucket, key, contentType=null) => {
  let params = { Bucket: bucket, Key: key, Expires: 10000}
  if (contentType) { params = { ...params, ContentType: contentType, }}
  try {
    return s3.getSignedUrl('putObject', params)
  } catch(e) {
    console.log("object err")
    throw e;
    throw new Error(e)
  }
}



const compareFaces = async (check_photo, profile_key) => {

  try {

    const user_photo_bucket =   process.env.USER_PHOTOS_BUCKET
    const check_photo_bucket =   process.env.QUARANTINE_CHECK_BUCKET    

    
    const rekognitionClient = new AWS.Rekognition();
    const params = {
        SourceImage: {
                        S3Object: {
                            Bucket: user_photo_bucket,
                            Name: profile_key
                        },
                    },
        TargetImage: {
                        S3Object: {
                            Bucket: check_photo_bucket,
                            Name: check_photo
                        },
                    },
        SimilarityThreshold: 70
    };

    let faceCompareRes = await rekognitionClient.compareFaces(params).promise();

    let comparisonResult;
    
    faceCompareRes.FaceMatches.forEach(data => {
        let position = data.Face.BoundingBox
        let similarity = data.Similarity
        if (!comparisonResult || data.Face.Confidence > comparisonResult.Confidence) {
          comparisonResult = data.Face;
        }
    })

    return comparisonResult;

  } catch(err) {
    console.log("compareFaces > err = ", err)
    throw err;
  }

}


module.exports = {
  getObjectUrl,
  getObjectUploadUrl,  
  compareFaces,
}