const moment = require('moment');
const _ = require("lodash")

const log = require('@genvis/genvis-log').childLogger('controllers:helpers')

const { UnauthorisedError } = require('../../utils/errors')
const config = require('../../cfg/baseConfig');

class ValidationError extends Error {
  constructor(args) {
      super(args);
      this.name = "ValidationError"
  }
}

const roundDate = (date, duration, method) => {
    return moment(Math[method]((+date) / (+duration)) * (+duration)); 
}

function verifyUserOwnsReportee ({ reportee_id, reportee, user, message, route }) {
  let ownsReportee = false;
  
  if (reportee && reportee.id === reportee_id) {
    ownsReportee = true;
  }

  if (!ownsReportee) {
    log.error({reportee, user}, `${route}: reportee_id=(${reportee_id}), user_id=(${user.id})`)
    throw new UnauthorisedError(
      message || 'Not authorised to perform the requested action',
      user,
    )
  }
  return ownsReportee;
}

// @TODO - should this also take reportee_id as a param
const errorResponse = ({res, log, err, message, status, logLevel, user_id }) => {

  if ( err.name.indexOf("AssertionError") >= 0 || err.name.indexOf("ValidationError") >= 0 || typeof err === 'string' ) {

    log.warn({ user_id, err }, message);    
    res.status(400);
    return res.send({ status: 'error', message: err.message })

  }else{            

    if (logLevel && logLevel === 'warn') {
      log.warn({ user_id, err }, message);
    } else {
      log.error({ user_id, err}, message);
    }

    res.status(status || 500);
    return res.send({
      message,
      status: 'error', 
      err, 
    })

  }            

}

const successResponse = ({ res, data, message, }) => {
  res.status(200)
  return res.send({
    data, 
    message,
    status: 'success',
  })
}




const mmss = (sec) => {
  const mm = '' + Math.trunc(sec / 60);
  const ss = sec % 60;
  return `${mm.length < 2 ? '0'+mm : mm}:${('0'+ss).slice(-2)}`;
}



/**
 * field required
 * @param value of field
 * @param name of field
 * @param errMessage
 */
function fieldRequired({ value, name, errMessage }) {
  
  if( value === undefined ){
    throw new ValidationError( errMessage || `${name} is required` );
  }

  return true;
}

/**
 * validate integer
 * @param number to validate
 * @param errMessage
 */
function validateInteger({ number, errMessage }) {
  
  if( !Number.isInteger(number) ){
    throw new ValidationError( errMessage || `The number (${number}) is not valid` );
  }

  return true;
}

/**
 * validate integer
 * @param number to validate
 * @param errMessage
 */
function validateBoolean({ value, errMessage }) {
  
  if( typeof value !== 'boolean' ){
    throw new ValidationError( errMessage || `The value (${value}) is not boolean type` );
  }

  return true;

}

function validateObject({ object, errMessage }) {
  
  console.log("typeof object === 'object' = ", (typeof object === 'object') );
  if( ! (typeof object === 'object') ){
    throw new ValidationError( errMessage || `The input is not an object` );
  }

  return true;
}

/**
 * validate timestamp
 * @param timestamp date in string want to validate
 * @param timestampFormat a parameter for testing
 * @param strict = true | false; 
 */
function validateTimestamp({ timestamp, strict = true, timestampFormat}) {
  
  let isValid = false;
  
  if(timestampFormat){
    isValid = moment(timestamp, timestampFormat, strict).isValid()
  }else{
    isValid = moment(timestamp, strict).isValid()
  }

  if( !isValid ){
    throw new ValidationError(`(${timestamp}) Invalid timestamp format${timestampFormat ? ` : must be ${timestampFormat} format` : ''} `);
  }

  return isValid;
}

/**
 * validate toTimestamp & fromTimestamp are valid timestamp and toTimestamp is after fromTimestamp
 * @param fromTimestamp date in string want to validate
 * @param toTimestamp date in string want to validate
 * @param timestampFormat a parameter for testing
 * @param strict = true | false; 
 */
function validateTimestampRange({ fromTimestamp, toTimestamp, strict = true, timestampFormat}) {
  
  validateTimestamp({ timestamp: fromTimestamp, strict, timestampFormat})
  validateTimestamp({ timestamp: toTimestamp, strict, timestampFormat})
  const isValid = moment(toTimestamp).isAfter( moment(fromTimestamp) )

  if( !isValid ){
    throw new ValidationError(`toTimestamp (${toTimestamp}) must be after fromTimestamp (${fromTimestamp})`);
  }

  return isValid;
}

module.exports = {
  roundDate,
  mmss,
  errorResponse,
  successResponse,
  verifyUserOwnsReportee,
  ValidationError,
  validateTimestamp,
  validateTimestampRange,
  validateInteger,
  fieldRequired,
  validateObject,
  validateBoolean,
}
