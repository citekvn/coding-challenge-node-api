const log = require('@genvis/genvis-log').childLogger('controllers:CovidTestController');
const _ = require('lodash')

const {
  verifyUserOwnsReportee,
  successResponse,
  errorResponse,
  validateInteger,
  fieldRequired,  
  validateBoolean,
} = require('./helpers')

const {
  UnauthorisedError    
} = require('../utils/errors');

let ReporteeService = null;
class ReporteeController {

  constructor({ ReporteeSVC }) {
    
    ReporteeService = ReporteeSVC ? ReporteeSVC : require('../services/ReporteeService');
    
  }

  async getReportees (req, res) {

    try {
      
      let reporteeService = new ReporteeService();
      let reportees = await reporteeService.getReportees({ reportee: req.reportee });

      return successResponse({
        res, data: reportees, message: 'Succesfully get reportee records.'
      });
      
    } catch(err) {

      return errorResponse({
        res, user_id, log, err, message: 'Could not get reportee record.'
      })
      
    }


  }


}

module.exports = ReporteeController;