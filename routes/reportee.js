const express                   = require('express');
const router                    = express.Router();
const { 
        bearerTokenMiddleware 
    }                           = require('../middleware/index');

const ReporteeController       = require('../controllers/ReporteeController');
const reporteeController       = new ReporteeController({});


router.get('/',                           bearerTokenMiddleware, reporteeController.getReportees);



module.exports = router;