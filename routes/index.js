const express     = require('express')
const requestIp   = require('request-ip')
const covidTestRoutes = require('./covidtest');
const reporteeRoutes = require('./reportee');

const healthCheck = (req, res) => {

  return res.send({
      message: 'successfully access reporting apis server',
      status: 'success',
      status_code: 200,
  })

}

const routes = (app) => {
  
  const router = express.Router();

  router.get('/',                                     healthCheck);

  app.use(requestIp.mw())

  app.use('/api/v1/covid-test', covidTestRoutes);
  app.use('/api/v1/reportees', reporteeRoutes);
  
  app.use('/api/v1', router);

}

module.exports = routes
