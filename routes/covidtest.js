const express                   = require('express');
const router                    = express.Router();
const { 
        reporteeMiddleware, 
        bearerTokenMiddleware 
    }                           = require('../middleware/index');

const CovidTestController       = require('../controllers/CovidTestController');
const covidTestController       = new CovidTestController({});



router.get('/',                           bearerTokenMiddleware, reporteeMiddleware, covidTestController.getCovidTests);

router.post('/',                          bearerTokenMiddleware, reporteeMiddleware, covidTestController.create );

router.put('/',                           bearerTokenMiddleware, reporteeMiddleware, covidTestController.update);

router.get('/clinics',                    bearerTokenMiddleware, covidTestController.getClinics);


module.exports = router;