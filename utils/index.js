const log = require("@genvis/genvis-log").childLogger("utils");

// @deprecated - this should be retired and replaced by shared module implementations - after which this package can be deleted.
// DB code is redundant
log.info(`utils > process.env.APP_ENVIRONMENT = ${process.env.APP_ENVIRONMENT}`);

//const { Middleware, Knex } = process.env.APP_ENVIRONMENT == 'PRODUCTION' || process.env.APP_ENVIRONMENT == 'STAGING' ? require('gvbelibs') : require('../../gvbelibs');
const libs = process.env.APP_ENVIRONMENT == 'PRODUCTION' || process.env.APP_ENVIRONMENT == 'STAGING' ? require('gvbelibs') : require('../../gvbelibs');

log.info({libs, }, ' loaded utils > libs = ');
module.exports = {
    Middleware: libs.Middleware,
    Knex: libs.Knex,
};