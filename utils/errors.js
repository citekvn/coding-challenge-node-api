class BaseError extends Error {
    constructor(message) {
        super(message);
        // Ensure the name of this error is the same as the class name
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}

/**
 * Base error class for errors where a reportee is involved.
 * adds a reportee_id field to context data
 */
class ReporteeError extends BaseError {
    constructor(message, reportee_id) {
        super(message);
        this.reportee_id = reportee_id;
        // this.name = "ReporteeError"
    }
}

class ReporteeNotFoundError extends BaseError {
    constructor(message, user_id, traveller_application_id) {
        super(message);
        this.user_id = user_id;
        this.traveller_application_id = traveller_application_id;
    }
}
/**
 * Denotes errors for requests which cannot be fulfilled until a reportee starts quarantine.
 */
class UninitializedReporteeError extends ReporteeError {
    constructor(message, reportee_id) {
        super(`${message} (id=${reportee_id})`, reportee_id);
        this.reportee_id = reportee_id;
        // this.name = "QuarantineNotStartedError"
    }
}

class UnauthorisedError extends BaseError {
  constructor(message, user, route) {
    super(message, user, route);
    this.user = user;
    this.route = route;
  }
}

module.exports = {
    ReporteeError,
    UninitializedReporteeError,
    ReporteeNotFoundError,
    UnauthorisedError
}