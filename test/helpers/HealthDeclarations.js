const request = require('supertest')
const momentRandom = require('moment-random')

const { requestWithAuth } = require('.')
const HealthDeclarationService = require('../../services/HealthDeclaration')

const HEALTH_DECLARATION_KEYS = [
  'Runny nose', 
  'Sore throat',
  'Temperature',
]

async function createHealthDeclaration(server, declaration, token) {
  let healthDeclarationRequest = request(server)
    .post('/api/v1/health-declarations')
    .send(declaration)
  return await requestWithAuth(healthDeclarationRequest, token);
}

async function getHealthDeclaration(databaseClient, id) {
  return await databaseClient('reportee_health_declarations')
    .where({ id, })
    .first();
}

function createRandomHealthDeclaration (reportee_id, created_by) {
  let created_at = momentRandom();
  let healthDeclarationService = new HealthDeclarationService();
  let health_declaration = HEALTH_DECLARATION_KEYS.map(declarationOption => ({
      text: declarationOption,
      value: Math.random() > .5,
    })
  )
  return ({
    created_at,
    created_by,
    reportee_id,
    health_declaration: JSON.stringify(health_declaration),
    health_declaration_status: healthDeclarationService.getDeclarationStatus(health_declaration),
  })
}

module.exports = {
  createHealthDeclaration,
  createRandomHealthDeclaration,
  getHealthDeclaration,
}