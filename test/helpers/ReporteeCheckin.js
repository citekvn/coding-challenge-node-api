const moment = require('moment')
const log = require('@genvis/genvis-log').childLogger('tests:helpers:ReporteeCheckin')

const ReporteeCheckin = require('../../models/ReporteeCheckin')
const ScheduleService = require('../../services/ScheduleService')

const createReporteeCheckins = (time, user_id, reportee_id) => {
  return {
    "notified_at" : time,
    "checkin_outcome_reason" : null,
    "created_at" : time,
    "checkin_outcome" : null,
    "checkin_at" : time,
    "user_id" : user_id,
    "reportee_id" : reportee_id,
    "schedule_at" : time,
    "created_by" : null,
    "updated_at" : time,
    "updated_by" : null,
    "checkin_failure_rationale" : null,
    "status" : null,
  }
}

async function createLateCheckinHandler({ 
  reportee, 
  reporteeUser, 
  databaseClient, 
  testCase,
}) {
  let reporteeCheckin = createReporteeCheckins(
    moment.utc(), 
    reportee.reportee_id,
    reporteeUser.id
  )
  let reporteeCheckinModel = new ReporteeCheckin();
  log.info({ reporteeCheckin }, `Creating checkin > ${testCase}`)
  let reporteeCheckinRecord = await reporteeCheckinModel.create({
    databaseClient,
    data: reporteeCheckin,
  })
  log.info({reporteeCheckinRecord}, `Created checkin schedule database record ${testCase}`);

  let timeout = 1000

  let scheduleService = ScheduleService.getInstance();
  await scheduleService.scheduleLateCheckinCheck({
    checkin_id: reporteeCheckinRecord.id,
    user: reporteeUser,
    timeout
  })

  return { timeout, reporteeCheckinRecord, reporteeCheckin }
}

module.exports = {
  createLateCheckinHandler,
  createReporteeCheckins,
}