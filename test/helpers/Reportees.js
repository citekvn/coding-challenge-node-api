const ReporteeData = require('../data/Reportee.json');

class ReporteeModel {
  constructor() {}

  async findByUserId ({ user_id, databaseClient }) {
    return await databaseClient('reportees')
      .where({ user_id, })
      .first();
  }

  async createReportee({ databaseClient, user_id }) {
    let data = {
      ...ReporteeData.reportee,
      user_id
    }

    let insertedRecord = await databaseClient('reportees')
      .insert(data)
      .returning('*')
    return insertedRecord[0]
  }
}

module.exports = ReporteeModel