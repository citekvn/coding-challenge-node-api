const request = require('supertest')
const { requestWithAuth } = require('./index')

async function getReporteeMessages(reporteeID, token, server) {
  let reporteeMessageRequest = request(server)
      .get(`/api/v1/reportee-messages?reportee_id=${reporteeID}`)
  return await requestWithAuth(reporteeMessageRequest, token)
}

module.exports = {
  getReporteeMessages,
}