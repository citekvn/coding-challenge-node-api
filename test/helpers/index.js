const axios       = require('axios')
const uuid        = require('uuid/v1')
const qs          = require('qs')
const Promise     = require('bluebird')
const nock        = require('nock')
const redis       = require('redis')
const moment      = require('moment')
const request     = require('supertest')

const config = require('../../cfg/config')

let OrganisationModel = require('./Organisations')

const log = require('@genvis/genvis-log').childLogger('tests:helpers:index')

const KnexPool = require('@genvis/genvis-knex')
KnexPool.init(config.db)

const UserData = require('../data/User.json')
const ReporteeData = require('../data/Reportee.json')
const TravellerData = require('../data/Traveller.json')

const { ReporteeModel } = require('./Reportees')
const { TravellerModel } = require('./Travellers')

const { Knex } = require('gvbelibs')

const { 
  getUsersDatabaseClient,
  getDatabaseClient
} = require('../../controllers/helpers/db')

let usersDatabaseClient = getUsersDatabaseClient();
let databaseClient = getDatabaseClient()
let travellerDatabaseClient = KnexPool.getKnex(process.env.POSTGRES_TEST_TRAVELLER_DB)

async function seedBaseReporteeData (mockSession) {
  let organisationModel = new OrganisationModel();
  organisation = await organisationModel.create({
    databaseClient: usersDatabaseClient,
    name: 'WAPOL',
  })

  let travellerModel = new TravellerModel();

  reporteeUser = await createUser(UserData.user, usersDatabaseClient);
  log.debug({ reporteeUser }, 'Created reportee user.')
  
  let travellerRecord = TravellerData.traveller;
  travellerRecord.user_id = reporteeUser.id;
  log.debug({ travellerRecord }, 'Creating traveller record.')
  traveller = await travellerModel.createTraveller({ 
    databaseClient: travellerDatabaseClient,
    traveller: travellerRecord,
  });

  let travellerApplicationRecord = TravellerData.traveller_application;
  travellerApplicationRecord.traveller_id = traveller.id;
  traveller_application = await travellerModel.createTravellerApplication({
    databaseClient: travellerDatabaseClient,
    travellerApplication: travellerApplicationRecord,
  });

  let data = ReporteeData.reportee
  data = {
    ...data,
    user_id: reporteeUser.id,
    traveller_id: traveller.id,
    traveller_application_id: traveller_application.id
  }

  let createReporteeResponse = await databaseClient('reportees')
    .insert(data)
    .returning('*')
  
  let reportee = createReporteeResponse[0];

  let userSession = {
    ...UserData.session_response,
    id: reporteeUser.id,
  }

  let nockScope;
  if (mockSession) {
    nockScope = mockUserSession(userSession);
  }

  return {
    userSession,
    nockScope,
    traveller_application,
    traveller,
    reporteeUser,
    reportee,
  }
}

const loginUser = async (username, password) => {
  const reqBody = qs.stringify({
    username,
    password,
    client_id: process.env.OAUTH_CLIENT,
    client_secret: 'secret',
    grant_type: 'password',
  })
  let response = await axios.post(`${config.oauthServer}/api/v1/login`, reqBody)
  return response.data;
}

async function userSession (bearer) {
  let response = await axios({
    method: 'GET',
    url: `${config.oauthServer}/api/v1/token/session`,
    headers: {
      Authorization: `Bearer ${bearer}`
    }
  })
  return response.data;
}

async function getRequest({server, route, bearer}) {
  let _request = request(server).get(route);
  return await requestWithAuth(_request, bearer);
}

let mockUserSession = (userResponse) => {
  let { id } = userResponse;
  let response = {
    ...UserData.session_response,
    id
  }
  return nock(config.oauthServer)
    .get('/api/v1/token/session')
    .reply(200, { user: response })
}

async function getReporteeFullRecords({
  reportee, 
  travellerDatabaseClient, 
  databaseClient
}) {
  let { user_id, traveller_id } = reportee;
  let reporteeModel = new ReporteeModel();
  let travellerModel = new TravellerModel();
  let traveller_record = await travellerModel.findTraveller({
    user_id, 
    databaseClient: travellerDatabaseClient,
    traveller_id,
  })
  let reportee_record = await reporteeModel.findByUserId({
    user_id,
    databaseClient,
  })
  log.info({ reportee_record }, 'Checked entity record.')
  let traveller_application_record = await travellerModel.findTravellerApplication({
    traveller_application_id: reportee_record.traveller_application_id,
    databaseClient: travellerDatabaseClient,
  })
  return { traveller_record, reportee_record, traveller_application_record, }
}

async function createReportee(reportee, databaseClient) {
  let response = await databaseClient('reportees')
    .insert(reportee)
    .returning('*')
  return response[0];
}

async function createUser (user, databaseClient) {
  let response = await databaseClient('users')
    .insert(user)
    .returning('*')
  return response[0];
}

async function momentEquals (date1, date2) {
  return moment(date1).format() === moment(date2).format();
}

async function clearDatabaseTables (tables, databaseClient) {
  return await Promise.map(tables, async table => {
    await databaseClient(table).del();
  })
}

async function requestWithAuth (request, bearer) {
  return await request
    .set('Authorization', `Bearer ${bearer}`)
}

function createRedisClient () {
  const client = redis.createClient();

  return client;

}

module.exports = { 
  loginUser, 
  createRedisClient,
  clearDatabaseTables, 
  requestWithAuth,
  userSession,
  createUser,
  createReportee,
  getReporteeFullRecords,
  mockUserSession,
  momentEquals,
  getRequest,
  seedBaseReporteeData
}
