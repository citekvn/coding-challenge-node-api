const moment = require('moment')

const log = require('@genvis/genvis-log').childLogger('tests:helpers:CheckedSchedule')

class Checkin {
  constructor() {}

  async create({ user_id, reportee_id, databaseClient, }) {
    let {checkin} = require('../data/Checkin.json');
    let timestamp = databaseClient.fn.now();
    let record = checkin;
    ['updated_at', 'schedule_at', 'created_at', 'notified_at'].forEach(key => {
      record[key] = timestamp;
    })
    record = { ...record, user_id, reportee_id}
    let createdRecord = await databaseClient('reportee_checkins')
      .insert(record)
      .returning('*')
    return createdRecord[0];
  }

  async find({ id, databaseClient }) {
    return await databaseClient('reportee_checkins').where({ id }).first();
  }
}

module.exports = Checkin;