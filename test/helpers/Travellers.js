class TravellerModel {
  constructor() {}

  async findTraveller ({ user_id, traveller_id, databaseClient }) {
    return await databaseClient('travellers')
      .where({ user_id, id: traveller_id, })
      .first()
  }

  async findTravellerApplication ({ traveller_application_id, databaseClient }) {
    return await databaseClient('traveller_applications')
      .where({ id: traveller_application_id, })
      .first()
  }

  async createTraveller ({ databaseClient, traveller }) {
    let response = await databaseClient('travellers')
      .insert(traveller)
      .returning('*')
    return response[0];
  }

  async createTravellerApplication ({ databaseClient, travellerApplication }) {
    let response = await databaseClient('traveller_applications')
      .insert(travellerApplication)
      .returning('*')
    return response[0];
  }
}

module.exports = { 
  TravellerModel,
}