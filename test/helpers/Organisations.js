class OrganisationsModel {
  constructor() {}

  async create({ databaseClient, name }) {
    let organisation_records = await databaseClient('organisations')
      .insert({ name, })
      .returning('*')
    return organisation_records[0];
  }
}

module.exports = OrganisationsModel;