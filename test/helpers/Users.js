let UserData = require('../data/User.json')

class UserModel {
  constructor() {}
  
  async createUser({ databaseClient }) {
    let data = {
      ...UserData.user,
    }

    let insertedRecord = await databaseClient('users')
      .insert(data)
      .returning('*')

    return insertedRecord[0];
  }
}

module.exports = UserModel