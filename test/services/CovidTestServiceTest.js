const expect = require("chai").expect;
const moment = require("moment");
// eslint-disable-next-line no-unused-vars
const log = require("@genvis/genvis-log").initLogging({name:"Service Utils Test", level: "info"});

const CovidTestService = require("../../services/CovidTestService");

class MockReporteeModel{
    async findByUserID({ user_id }){
        if(user_id === 3){
            return Promise.resolve( { id: 3 } )
        }else{
            return Promise.resolve( null )
        }
        
    }
}

class MockCovidTestModel{
    async create({ record }){
        return Promise.resolve( record )        
    }    
}

const MockCovidTestService  = CovidTestService.props({ ReporteeModel: MockReporteeModel, CovidTestModel: MockCovidTestModel });

describe("Reporting Service Tests", function () {
    
    describe('#checkinsReport', function () {

        it("should throw the error if cannt find the reportee by user_id", async function () {
                    
            const mockService = MockCovidTestService.getInstance();
            const reportee_id = null;
            const positive_test = false
            const clinic_id = 1
            const user = { id: 1 };

            try{
                const createdCovidTest = await mockService.create({ reportee_id, positive_test, clinic_id, user });
            }catch(err){
                expect(err.toString()).equal('Error: No reportee associated with user 1')
            }
            
        });

        it("should create the covid test if can find the reportee by user_id and reportee is null", async function () {
                    
            const mockService = MockCovidTestService.getInstance();
            const reportee_id = null;
            const positive_test = false
            const clinic_id = 1
            const user = { id: 3 };

            const createdCovidTest = await mockService.create({ reportee_id, positive_test, clinic_id, user });

            console.log('createdCovidTest = ', createdCovidTest);

            expect(createdCovidTest).be.an('object'); 
            expect(createdCovidTest).have.property('reportee_id');
            expect(createdCovidTest.reportee_id).equal(3)
            expect(createdCovidTest).have.property('positive_test');
            expect(createdCovidTest).have.property('clinic_id');
            expect(createdCovidTest).have.property('created_by');
            expect(createdCovidTest).have.property('created_at');
            expect(createdCovidTest).have.property('updated_at');
            
        });

        it("should create the covid test if reportee_id is not null", async function () {
                    
            const mockService = MockCovidTestService.getInstance();
            const reportee_id = 1;
            const positive_test = false
            const clinic_id = 1
            const user = { id: 3 };

            const createdCovidTest = await mockService.create({ reportee_id, positive_test, clinic_id, user });

            console.log('createdCovidTest = ', createdCovidTest);

            expect(createdCovidTest).be.an('object'); 
            expect(createdCovidTest).have.property('reportee_id');
            expect(createdCovidTest.reportee_id).equal(1)
            expect(createdCovidTest).have.property('positive_test');
            expect(createdCovidTest).have.property('clinic_id');
            expect(createdCovidTest).have.property('created_by');
            expect(createdCovidTest).have.property('created_at');
            expect(createdCovidTest).have.property('updated_at');
            
        });

    });

});


