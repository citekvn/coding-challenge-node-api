const log = require('@genvis/genvis-log').initLogging({
  level: 'debug',
  name: 'test:auth'
})

const { expect } = require('chai')
let GenvisAuthMiddleware = require('../../genvis-auth-middleware/index')

let genvisAuthMiddleware = new GenvisAuthMiddleware();

describe('Returns correct permission on validate roles', function() {

  it ('Returns permitted = false when valid role name not supplied', function() {
    let role_names = ['Traveller']
    let permitted_roles = ['Compliance.Admin']

    let permitted = genvisAuthMiddleware.validateRoles(role_names, permitted_roles)

    expect(permitted).to.equal(false)
  })

  it ('Returns permitted = true with valid role specified', function() {
    let role_names = ['Compliance.Admin']
    let permitted_roles = ['Compliance.Admin']

    let permitted = genvisAuthMiddleware.validateRoles(role_names, permitted_roles)

    expect(permitted).to.equal(true)
  })

})