process.env.GENVIS_ENV='dev';

const log = require("@genvis/genvis-log").initLogging({
    name: "test:CtrlHelpers",
    prettyPrint: true,
    level: 'debug' 
  })
const { expect } = require('chai');
const {validateTimestamp, validateTimestampRange} = require('../../../controllers/helpers');

describe('Helpers test', () => {

    describe("#validateTimestamp", function () {

        it("return true if date in ISO_8601 format", function () {

            const isValid = validateTimestamp({ timestamp: '2021-01-01T12:00:00.000Z'})
            expect(isValid).to.be.true;

        });

        it("return true if date in DD/MM/YYYY HH:mm:ss format", function () {

            const isValid = validateTimestamp({timestamp: '01/01/2021 12:00:00', timestampFormat: 'DD/MM/YYYY HH:mm:ss'})
            expect(isValid).to.be.true;

        });


        it("should throw the error if wrong format", function () {
            expect( () => {
                validateTimestamp({timestamp: '01/01/2021'})
            }).to.be.throw('(01/01/2021) Invalid timestamp format')
        });

        it("should throw the error if wrong format YYYY/MM/DD", function () {
            expect( () => {
                validateTimestamp({timestamp: '01/01/2021', timestampFormat: 'YYYY/MM/DD'})
            }).to.be.throw('(01/01/2021) Invalid timestamp format : must be YYYY/MM/DD format')
        });        

    });

    describe("#validateTimestampRange", function () {

        it("return true if fromTimestamp is before toTimestamp", function () {

            const isValid = validateTimestampRange({ fromTimestamp: '2021-01-01T12:00:00.000Z', toTimestamp: '2021-01-02T12:00:00.000Z'})
            expect(isValid).to.be.true;

        });


        it("return false if fromTimestamp is after toTimestamp", function () {
            expect( () => {
                validateTimestampRange({ fromTimestamp: '2021-01-02T12:00:00.000Z', toTimestamp: '2021-01-01T12:00:00.000Z'})
            }).to.be.throw('toTimestamp (2021-01-01T12:00:00.000Z) must be after fromTimestamp (2021-01-02T12:00:00.000Z)')
        });

        it("return false if fromTimestamp is same toTimestamp", function () {
            expect( () => {
                validateTimestampRange({ fromTimestamp: '2021-01-01T12:00:00.000Z', toTimestamp: '2021-01-01T12:00:00.000Z'})
            }).to.be.throw('toTimestamp (2021-01-01T12:00:00.000Z) must be after fromTimestamp (2021-01-01T12:00:00.000Z)')
        });

    });

})