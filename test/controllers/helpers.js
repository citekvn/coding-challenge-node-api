class MockExpressResponse {
  constructor() {}

  status(code) {
    this.statusCode = code;
  }

  send(response) {
    this.response = response;
    return this;
  }

  json(response) {
    this.response = response;
    return this;
  }
}

module.exports = {
  MockExpressResponse
}