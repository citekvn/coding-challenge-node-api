const log = require("@genvis/genvis-log").initLogging({
  name: "test:CovidTestController",
  prettyPrint: true,
  level: 'debug' 
})

const { expect } = require("chai");

const { MockExpressResponse } = require('./helpers');

const mockLog = {
  info: () => {},
  warn: () => {},
  error: () => {},
};

const mockCovidTests = [ { id: 1 } ];
const mockCreatedCovidTest = { id: 1 };

class MockCovidTestService{
  
  async getCovidTests({}){

    return Promise.resolve(
      mockCovidTests
    );

  } 

  async create({}){

    return Promise.resolve(
      mockCreatedCovidTest
    );

  }   

  async update({}){

    return Promise.resolve(
      mockCreatedCovidTest
    );

  }     

};

const CovidTestController = require("../../controllers/CovidTestController")

const covidTestController = new CovidTestController({CovidTestSVC: MockCovidTestService });

const tokenSession = { data: { user: {} }};

describe("Covid Test controller tests", function() {


  describe("#getCovidTests", function() {

    it ("Should return a 401 if not reportee object info in req", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{

        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.getCovidTests(req, response)
      
      expect(controllerResponse.statusCode).to.eq(401);
      expect(controllerResponse.response.message).to.eq('Unauthorised');
  
    })

    it ("Should return a 200 and data if reportee info in req", async function () {
  
      const req = {
        
        tokenSession,
        reportee: {},
        query: {
        },

        body:{

        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.getCovidTests(req, response)

      expect(controllerResponse.statusCode).to.eq(200);
      expect(controllerResponse.response.message).to.eq('Succesfully get covid test records.');
      expect(controllerResponse.response.data).lengthOf( mockCovidTests.length );

    })

    
  })


  describe("#create", function() {

    it ("Should return a 400 if reportee_id is not in body", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{
          reportee_id: 1,
          clinic_id: 2
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.create(req, response)

      expect(controllerResponse.statusCode).to.eq(400);
      expect(controllerResponse.response.message).to.eq('positive_test value is not boolean');
  
    })

    it ("Should return a 400 if positive_test is not in body", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{
          reportee_id: 1,
          positive_test: true
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.create(req, response)      

      expect(controllerResponse.statusCode).to.eq(400);
      expect(controllerResponse.response.message).to.eq('clinic_id value is not integer');
  
    })

    it ("Should return a 400 if positive_test is not in body", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{
          clinic_id: 2,
          positive_test: true
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.create(req, response)
      
      expect(controllerResponse.statusCode).to.eq(400);
      expect(controllerResponse.response.message).to.eq('reportee_id value is not integer');
  
    })    

    it ("Should return a 401 if reportee_id does not match reportee from req", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{
          reportee_id: 1,
          clinic_id: 2,
          positive_test: true
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.create(req, response)
      
      expect(controllerResponse.statusCode).to.eq(401);
      expect(controllerResponse.response.message).to.eq('Unauthorised');
  
    })        
    
    it ("Should return a 200 if reportee_id does not match reportee from req", async function () {
  
      const req = {
        
        tokenSession,
        reportee: { id: 3},
        query: {
        },

        body:{
          reportee_id: 3,
          clinic_id: 2,
          positive_test: true
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.create(req, response)
      
      console.log('controllerResponse = ', controllerResponse);

      expect(controllerResponse.statusCode).to.eq(200);
      expect(controllerResponse.response.message).to.eq('Succesfully created covid test record.');
      expect(controllerResponse.response.data).deep.equal(mockCreatedCovidTest);
  
    })        

  })  
  
  
  describe("#update", function() {

    it ("Should return a 400 if reportee_id is not in body", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.update(req, response)

      expect(controllerResponse.statusCode).to.eq(400);
      expect(controllerResponse.response.message).to.eq('id value is not integer');
  
    })

    it ("Should return a 400 if positive_test is not in body", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{
          id: 1,
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.update(req, response)      

      expect(controllerResponse.statusCode).to.eq(400);
      expect(controllerResponse.response.message).to.eq('positive_test value is not boolean');
  
    })



    it ("Should return a 401 if reportee_id does not match reportee from req", async function () {
  
      const req = {
        
        tokenSession,

        query: {
        },

        body:{
          id: 1,
          positive_test: true
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.update(req, response)
      
      expect(controllerResponse.statusCode).to.eq(401);
      expect(controllerResponse.response.message).to.eq('Unauthorised');
  
    })        
    
    it ("Should return a 200 if reportee_id does not match reportee from req", async function () {
  
      const req = {
        
        tokenSession,
        reportee: { id: 3},
        query: {
        },

        body:{
          id: 3,
          positive_test: true
        },
      }
  
      const response = new MockExpressResponse();
      const controllerResponse = await covidTestController.update(req, response)
      
      expect(controllerResponse.statusCode).to.eq(200);
      expect(controllerResponse.response.message).to.eq('Successfully updated COVID test record.');
      expect(controllerResponse.response.data).deep.equal(mockCreatedCovidTest);
  
    })        

  })  
  
  


})