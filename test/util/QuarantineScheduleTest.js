const expect = require("chai").expect;

// eslint-disable-next-line no-unused-vars
const log = require("@genvis/genvis-log").initLogging({name:"Service Utils Test", level: "info", prettyPrint: true});
const moment = require("moment");
const QuarantineSchedule = require("../../util/QuarantineSchedule");
const config = require("../../cfg/config");

const QUARANTINE_START = "2020-08-16";
const DATE_FORMAT = "YYYY-MM-DDTHH:mm";

function createSchedule(scheduleName) {
    const schedule = new QuarantineSchedule({
        name: "repeating",
        scheduleDef: config.schedule.schedules[scheduleName],
        startDate: moment(QUARANTINE_START).startOf("day"),
        days: 14
    }).generate();
    return schedule;
}

describe("Quarantine Schedule tests", function () {
    describe("#generate", function () {
        it("verify low risk schedule is generated correctly", function () {
            const schedule = createSchedule("low").getSchedule();

            expect(schedule).to.have.length(5);
            expect(schedule[0].format(DATE_FORMAT)).to.equal("2020-08-16T10:00");
            expect(schedule[1].format(DATE_FORMAT)).to.equal("2020-08-18T14:00");
            expect(schedule[2].format(DATE_FORMAT)).to.equal("2020-08-22T18:00");
            expect(schedule[3].format(DATE_FORMAT)).to.equal("2020-08-26T14:00");
            expect(schedule[4].format(DATE_FORMAT)).to.equal("2020-08-29T10:00");
        });

        it("verify low risk schedule with daysIn is generated correctly", function () {
            const schedule = new QuarantineSchedule({
                name: "low",
                scheduleDef: config.schedule.schedules.low,
                startDate: moment(QUARANTINE_START).startOf("day"),
                daysIn: 4
            }).generate({}).getSchedule();
            expect(schedule).to.have.length(3);
            expect(schedule[0].format(DATE_FORMAT)).to.equal("2020-08-22T18:00");
            expect(schedule[1].format(DATE_FORMAT)).to.equal("2020-08-26T14:00");
            expect(schedule[2].format(DATE_FORMAT)).to.equal("2020-08-29T10:00");
        });

        it("verify medium risk schedule is generated correctly", function () {
            const schedule = createSchedule("medium").getSchedule();
            expect(schedule).to.have.length(9);
        });


        it("verify medium risk schedule with daysIn is generated correctly", function () {
            const schedule = new QuarantineSchedule({
                name: "medium",
                scheduleDef: config.schedule.schedules.medium,
                startDate: moment(QUARANTINE_START).startOf("day"),
                daysIn: 10
            }).generate().getSchedule();
            expect(schedule).to.have.length(2);
        });

        it("verify high risk schedule is generated correctly", function () {
            const schedule = createSchedule("high").getSchedule();
            expect(schedule).to.have.length(10);
            expect(schedule[4].format(DATE_FORMAT)).to.equal("2020-08-21T09:00");
            expect(schedule[5].format(DATE_FORMAT)).to.equal("2020-08-21T21:00");
        });

        it("verify high risk schedule with daysIn is generated correctly", function () {
            const schedule = new QuarantineSchedule({
                name: "high",
                scheduleDef: config.schedule.schedules.high,
                startDate: moment(QUARANTINE_START).startOf("day"),
                daysIn: 10
            }).generate().getSchedule();
            expect(schedule).to.have.length(3);
            expect(schedule[0].format(DATE_FORMAT)).to.equal("2020-08-26T09:00");
            expect(schedule[1].format(DATE_FORMAT)).to.equal("2020-08-26T18:00");
            expect(schedule[2].format(DATE_FORMAT)).to.equal("2020-08-29T19:00");
        });

        it("verify repeating schedule is generated correctly", function () {
            const schedule = createSchedule("repeating").getSchedule();
            expect(schedule).to.have.length(6);
            expect(schedule[0].format(DATE_FORMAT)).to.equal("2020-08-16T01:10");
            expect(schedule[1].format(DATE_FORMAT)).to.equal("2020-08-16T01:20");
            expect(schedule[2].format(DATE_FORMAT)).to.equal("2020-08-16T01:30");
            expect(schedule[3].format(DATE_FORMAT)).to.equal("2020-08-16T01:40");
            expect(schedule[4].format(DATE_FORMAT)).to.equal("2020-08-16T03:20");
            expect(schedule[5].format(DATE_FORMAT)).to.equal("2020-08-16T03:40");

        });
    });

    describe("#getCurrentDay", function () {
        it("verify dates before and after schedule period are marked as not in schedule", function () {
            const schedule = createSchedule("low");
            const before = moment(QUARANTINE_START).subtract('1', 'day');
            expect(schedule.getCurrentDay(before)).to.equal(-1);

            const after = moment(QUARANTINE_START).add('15', 'day');
            expect(schedule.getCurrentDay(after)).to.equal(-1);
        });

        it("verify day 1 is marked as in schedule", function () {
            const schedule = createSchedule("low");
            const day1 = moment(QUARANTINE_START).add('1', 'hour');
            expect(schedule.getCurrentDay({ now: day1})).to.equal(1);
            day1.add('22', 'hours');
            expect(schedule.getCurrentDay({ now: day1})).to.equal(1);
        });

        it("verify day 2 is marked as in schedule", function () {
            const schedule = createSchedule("low");
            const day1 = moment(QUARANTINE_START).add(1, 'day').add('1', 'hour');
            expect(schedule.getCurrentDay({ now: day1})).to.equal(2);
            day1.add('22', 'hours');
            expect(schedule.getCurrentDay({ now: day1})).to.equal(2);
        });

        it("verify day 13 is marked as in schedule", function () {
            const schedule = createSchedule("low");
            const day1 = moment(QUARANTINE_START).add(12, 'day').add('1', 'hour');
            expect(schedule.getCurrentDay({ now: day1})).to.equal(13);
            day1.add('22', 'hours');
            expect(schedule.getCurrentDay({ now: day1})).to.equal(13);
        });

        it("verify day 14 is marked as in schedule", function () {
            const schedule = createSchedule("low");
            const day14 = moment(QUARANTINE_START).add('13', 'days').add(1, 'hour');
            expect(schedule.getCurrentDay({ now: day14 })).to.equal(14);
            day14.add('22', 'hours').add(59, 'minutes');
            expect(schedule.getCurrentDay({ now: day14})).to.equal(14);
            day14.add('2', 'minutes');
            log.info({day14}, "Day 14 is");
            expect(schedule.getCurrentDay({ now: day14})).to.equal(-1);
        });
    });

    describe("#isInQuarantine", function () {
       it("verify in quarantine dates are reported correctly", function () {
           const schedule = createSchedule("low");
           const now = moment(QUARANTINE_START).add('1', 'hour');

           expect(schedule.isInQuarantine({now})).to.be.true;

           now.add(9, 'days');
           expect(schedule.isInQuarantine({now})).to.be.true;

           now.add(4, 'days');
           expect(schedule.isInQuarantine({now})).to.be.true;

           now.add(22, 'hours');
           now.add(59, 'minutes');
           expect(schedule.isInQuarantine({now})).to.be.true;

           now.add(2, 'minutes');
           // now.add(1, 'day');
           expect(schedule.isInQuarantine({now})).to.be.false;
       });
    });
});