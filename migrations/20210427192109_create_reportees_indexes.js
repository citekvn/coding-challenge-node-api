exports.up = function(knex) {
  
  let createIndexes = 
  `
    SET lock_timeout TO '2s';
    Create index reportees_quarantine_date_idx on reportees ( quarantine_start, quarantine_end);
    Create index reportee_reference_images_reportee_id_idx on reportee_reference_images ( reportee_id );
    
  `
  
  return knex.schema.raw(createIndexes)

};

exports.down = function(knex) {
  let dropIndexes = 
  `
    SET lock_timeout TO '2s';
    DROP INDEX IF EXISTS reportees_quarantine_date_idx;
    DROP INDEX IF EXISTS reportee_reference_images_reportee_id_idx;
  `
  
  return knex.schema.raw(dropIndexes)
};