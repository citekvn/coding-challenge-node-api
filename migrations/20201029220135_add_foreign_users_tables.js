exports.up = function(knex, Promise) {

    return knex.schema.raw(`
                            CREATE EXTENSION IF NOT EXISTS postgres_fdw;
                            CREATE SERVER foreign_users_db FOREIGN DATA WRAPPER postgres_fdw OPTIONS ( dbname 'users');
                            CREATE FOREIGN TABLE IF NOT EXISTS foreign_users (  
                              id integer, 
                              username varchar(200), 
                              email varchar(200), 
                              first_name varchar(200), 
                              last_name varchar(200),
                              dob DATE
                            )
                            SERVER foreign_users_db  
                            OPTIONS (schema_name 'public', table_name 'users');                          
                            `);
  
  
  };
  
  exports.down = function(knex, Promise) {
    
  
    return knex.schema.raw(`
                            DROP FOREIGN TABLE IF EXISTS foreign_users;
                            DROP SERVER IF EXISTS foreign_users_db;
                            DROP EXTENSION IF EXISTS postgres_fdw;
                            `);
    
  
  };