
exports.up = function(knex) {
  return knex.schema.createTable('reportee_notes', table => {
    table.increments();
    table.integer('reportee_id');
    table.specificType('note', 'text')
    // lack of update routes is intentional. notes not permitted as editable.
    table.integer('created_by');
    table.timestamp('created_at');
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_notes')
};
