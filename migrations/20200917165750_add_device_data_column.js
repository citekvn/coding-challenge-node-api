
exports.up = function(knex) {
  return knex.schema.table('reportee_checkin_events', t => {
    t.json('device_data')
    t.json('network_data')
    t.json('location_data')
  })
}; 

exports.down = function(knex) {
  return knex.schema.table('reportee_checkin_events', t => {
    t.dropColumn('device_data')
    t.dropColumn('network_data')
    t.dropColumn('location_data')
  })
};
