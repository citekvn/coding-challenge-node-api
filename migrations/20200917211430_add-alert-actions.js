
exports.up = function(knex) {
  return knex.schema.createTable('alert_actions', table => {
    table.increments()
    table.integer('reportee_id')
    table.integer('parent_id')
    table.boolean('alert_acknowledged')
    table.integer('created_by')
    table.integer('updated_by')
    table.string('alert_type', 50)
    table.string('alert_action')
    table.timestamp('created_at')
    table.timestamp('updated_at')
    table.specificType('alert_note', 'text')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('alert_actions')
};
