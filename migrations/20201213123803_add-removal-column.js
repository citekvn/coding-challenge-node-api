
exports.up = function(knex) {
  return knex.schema.table('reportees', function (table) {
    table.boolean('removed_from_quarantine');
  })
};

exports.down = function(knex) {
  return knex.schema.table('reportees', function (table) {
    table.dropColumn('removed_from_quarantine');
  })
};
