
exports.up = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE reportees
      ALTER COLUMN category TYPE varchar(25)
  `)
};

exports.down = function(knex) {
  knex.schema.raw(`
    ALTER TABLE reportees
      ALTER COLUMN category TYPE int
  `)
};
