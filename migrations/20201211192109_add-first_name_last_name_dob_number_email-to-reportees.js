exports.up = function(knex) {
  
  let addColumns = 
  `
    SET lock_timeout TO '2s';
    ALTER TABLE reportees 
    ADD COLUMN IF NOT EXISTS first_name varchar(255), 
    ADD COLUMN IF NOT EXISTS last_name varchar(255),
    ADD COLUMN IF NOT EXISTS dob date,
    ADD COLUMN IF NOT EXISTS number varchar(255),
    ADD COLUMN IF NOT EXISTS email varchar(255);    
  `
  
  return knex.schema.raw(addColumns)

};

exports.down = function(knex) {
  let dropColumns = 
  `
    SET lock_timeout TO '2s';
    ALTER TABLE reportees 
    DROP COLUMN IF EXISTS first_name,
    DROP COLUMN IF EXISTS last_name,
    DROP COLUMN IF EXISTS dob,
    DROP COLUMN IF EXISTS number,
    DROP COLUMN IF EXISTS email;
  `
  
  return knex.schema.raw(dropColumns)
};