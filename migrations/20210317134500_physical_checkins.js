
exports.up = function(knex) {
  return knex.schema.createTable('physical_checkins', t => {
    
    t.increments()
    t.timestamp('checkin_date')
    t.integer('diff')
    t.integer('created_by')
    t.timestamp('created_at').defaultTo(knex.fn.now())

  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('physical_checkins')
};
