
exports.up = function(knex) {
  return knex.schema.table('reportees', function (table) {
    table.timestamp('removed_from_quarantine_at');
  })
};

exports.down = function(knex) {
  return knex.schema.table('reportees', function (table) {
    table.dropColumn('removed_from_quarantine_at');
  })
};
