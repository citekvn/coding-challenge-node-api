
exports.up = function(knex) {
  return knex.schema.createTable('reportee_health_declarations', t => {
    t.increments()
    t.integer('reportee_id')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.integer('created_by')
    t.boolean('health_declaration_status')
    t.specificType('health_declaration', 'json')
  })
  
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_health_declarations')
};
