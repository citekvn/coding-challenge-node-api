
exports.up = function(knex) {
  return knex.schema.createTable('reportee_message_reads', t => {
    t.increments()
    t.integer('message_id')
    t.integer('user_id')
    t.boolean('read_status')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.timestamp('updated_at').defaultTo(knex.fn.now())
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_message_reads')
};
