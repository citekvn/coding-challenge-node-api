
exports.up = function(knex) {
  return knex.schema.createTable('reportee_messages', t => {
    t.increments('id')
    t.integer('reportee_id')
    t.timestamp('delivered_at')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.integer('created_by')
    t.specificType('message', 'text')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_messages')
};
