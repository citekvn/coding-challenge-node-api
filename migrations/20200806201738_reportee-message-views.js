
exports.up = function(knex) {
  return knex.schema.createTable('reportee_message_views', t => {
    t.increments()
    t.integer('message_id')
    t.integer('viewed_by')
    t.timestamp('viewed_at').defaultTo(knex.fn.now())
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_message_views')
};
