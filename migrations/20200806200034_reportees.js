
exports.up = function(knex) {
  return knex.schema.createTable('reportees', t => {
    t.increments()
    t.integer('user_id')
    t.integer('traveller_id')
    t.integer('traveller_application_id')
    t.integer('organisation_id')
    t.integer('created_by')
    t.integer('updated_by')
    t.string('organisation_name', 25)
    t.string('quarantine_address')
    t.string('quarantine_address_long', 50)
    t.string('quarantine_address_lat', 50)
    t.integer('quarantine_days')
    t.timestamp('quarantine_start')
    t.timestamp('quarantine_end')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.timestamp('updated_at').defaultTo(knex.fn.now())
    t.timestamp('consent_given')
    t.integer('category')
    t.specificType('device_information', 'json')
    t.specificType('network_information', 'json')
    t.specificType('consent', 'json')
    t.specificType('app_permissions', 'json')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportees')
};
