
exports.up = function(knex) {
  return knex.schema.createTable('reportee_reference_images', t => {
    t.increments()
    t.integer('reportee_id')
    t.string('image_key')
    t.boolean('reference_image')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.timestamp('updated_at').defaultTo(knex.fn.now())
    t.specificType('image_quality', 'json')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_reference_images')
};
