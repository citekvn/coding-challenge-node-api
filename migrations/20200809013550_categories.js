
exports.up = function(knex) {
  return knex.schema.createTable('categories', t => {
    t.increments();
    t.string('name', 50)
    t.string('color', 25)
    t.string('description')
    t.integer('created_by')
    t.integer('updated_by')
    t.timestamp('created_at')
    t.timestamp('updated_at')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('categories')
};
