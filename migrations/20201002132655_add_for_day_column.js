
exports.up = function(knex) {
  return knex.schema.table("reportee_covid_tests", (table) => {
  	table.integer("day_specifier")
  })
};

exports.down = function(knex) {
  return knex.schema.table("reportee_covid_tests", (table) => {
  	table.dropColumn("day_specifier")
  })
};
