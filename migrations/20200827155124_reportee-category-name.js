
exports.up = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE reportees
      RENAME COLUMN category TO risk_category;
  `)
};

exports.down = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE reportees
      RENAME COLUMN risk_category TO category
  `)
};
