exports.up = function(knex, Promise) {

    return knex.schema.raw(`
                            CREATE USER MAPPING FOR g2gadmin  
                            SERVER foreign_users_db  
                            OPTIONS (user 'g2gadmin', password '${process.env.POSTGRES_PASSWORD}');  
                            `);
  
  
  };
  
  exports.down = function(knex, Promise) {
    
  
    return knex.schema.raw(`
                          DROP USER MAPPING IF EXISTS FOR g2gadmin SERVER foreign_users_db
                            `);
    
  
  };