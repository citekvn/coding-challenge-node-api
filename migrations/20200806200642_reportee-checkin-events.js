
exports.up = function(knex) {
  return knex.schema.createTable('reportee_checkin_events', t => {
    t.increments()
    t.integer('reportee_id')
    t.integer('checkin_id')
    t.integer('created_by')
    t.string('checkin_face_key')
    t.string('checkin_long', 50)
    t.string('checkin_lat', 50)
    t.float('similarity')
    t.boolean('status')
    t.timestamp('checkin_at')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.timestamp('updated_at').defaultTo(knex.fn.now())
    t.float('checkin_radius')
    t.boolean('location_status')
    t.string('status_reason')
  })  
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_checkin_events')
};
