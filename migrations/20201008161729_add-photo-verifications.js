
exports.up = function(knex) {
  return knex.schema.createTable('photo_verifications', function(table) {
    table.increments();
    table.integer('reportee_id')
    table.integer('reference_image')
    table.boolean('verification')
    table.integer('verified_by')
    table.integer('updated_by')
    table.timestamp('verified_at')
    table.timestamp('updated_at')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('photo_verifications')
};
