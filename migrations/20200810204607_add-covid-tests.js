
exports.up = function(knex) {
  return knex.schema.createTable('reportee_covid_tests', t => {
    t.increments();
    t.integer('reportee_id')
    t.boolean('positive_test')
    t.integer('clinic_id')
    t.timestamp('testing_timestamp')
    t.timestamp('created_at')
    t.timestamp('updated_at')
    t.integer('created_by')
    t.integer('updated_by')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_covid_tests')
};
