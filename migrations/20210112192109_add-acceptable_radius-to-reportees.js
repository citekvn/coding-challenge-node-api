exports.up = function(knex) {
  
  let addColumns = 
  `
    SET lock_timeout TO '2s';
    ALTER TABLE reportees 
    ADD COLUMN IF NOT EXISTS acceptable_radius integer;    
  `
  
  return knex.schema.raw(addColumns)

};

exports.down = function(knex) {
  let dropColumns = 
  `
    SET lock_timeout TO '2s';
    ALTER TABLE reportees 
    DROP COLUMN IF EXISTS acceptable_radius;
  `
  
  return knex.schema.raw(dropColumns)
};