
exports.up = function(knex) {
  return knex.schema.createTable('alerts', table => {
    table.increments()
    table.integer('user_id')
    table.integer('reportee_id')
    table.integer('parent_event_id')
    table.integer('created_by')
    table.integer('updated_by')
    table.string('alert_type', 50)
    table.string('alert_subtype', 50)
    table.dateTime('created_at').defaultTo(knex.fn.now())
    table.dateTime('updated_at').defaultTo(knex.fn.now())
    table.text('alert_information')
    table.json('alert_error')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('alerts')
};
