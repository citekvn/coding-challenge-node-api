exports.up = function(knex, Promise) {

  return knex.schema.raw(`
                          CREATE INDEX IF NOT EXISTS reportees_user_id_idx ON reportees(user_id);
                          CREATE INDEX IF NOT EXISTS reportee_checkin_events_reportee_id_idx ON reportee_checkin_events(reportee_id);       
                          CREATE INDEX IF NOT EXISTS reportee_checkins_reportee_id_idx ON reportee_checkins(reportee_id);       
                          CREATE INDEX IF NOT EXISTS reportee_health_declarations_reportee_id_idx ON reportee_health_declarations(reportee_id);    
                          CREATE INDEX IF NOT EXISTS reportee_covid_tests_reportee_id_idx ON reportee_covid_tests(reportee_id);       
                          `);


};

exports.down = function(knex, Promise) {
  

  return knex.schema.raw(`
                          drop index reportees_user_id_idx;
                          drop index reportee_checkin_events_reportee_id_idx;
                          drop index reportee_checkins_reportee_id_idx;
                          drop index reportee_health_declarations_reportee_id_idx;
                          drop index reportee_covid_tests_reportee_id_idx;
                          `);
  

};
