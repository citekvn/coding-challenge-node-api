
exports.up = function(knex) {
  return knex.schema.createTable('reportee_arrival_declarations', function(table) {
    table.increments();
    table.integer('reportee_id')
    table.string('declaration_lat', 50)
    table.string('declaration_lng', 50)
    table.string('registered_address_lat', 50)
    table.string('registered_address_lng', 50)
    table.float('registered_location_distance')
    table.timestamps(true, true)
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_arrival_declarations')
};
