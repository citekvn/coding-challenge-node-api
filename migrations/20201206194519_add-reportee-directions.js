
exports.up = function(knex) {
  return knex.schema.table('reportees', t => {
    t.string('direction')
  })
};

exports.down = function(knex) {
  return knex.schema.table('reportees', t => {
    t.dropColumn('direction')
  })
};
