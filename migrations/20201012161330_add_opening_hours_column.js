
exports.up = function(knex) {
  return knex.schema.table("covid_clinics", (table) => {
  	table.string("clinic_opening_hours")
  })
};

exports.down = function(knex) {
  return knex.schema.table("covid_clinics", (table) => {
  	table.dropColumn("clinic_opening_hours")
  })
};
