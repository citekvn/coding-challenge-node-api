
exports.up = function(knex) {
  return knex.schema.createTable('liveness_checks', table => {
    table.increments()
    table.integer('checkin_id')
    table.integer('checkin_event_id')
    table.integer('sequence_id')
    table.integer('created_by')
    table.boolean('success')
    table.string('liveness_check_method')
    table.string('image_key')
    table.timestamp('created_at')
    table.json('liveness_check_response')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('liveness_checks')
};
