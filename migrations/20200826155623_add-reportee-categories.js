
exports.up = function(knex) {
  return knex.schema.createTable('user_categories', t => {
    t.increments();
    t.integer('user_id')
    t.integer('reportee_id')
    t.integer('category_id')
    t.integer('created_by')
    t.integer('updated_by')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.timestamp('updated_at').defaultTo(knex.fn.now())
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('user_categories')
};
