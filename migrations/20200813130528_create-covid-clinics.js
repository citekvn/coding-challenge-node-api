
exports.up = function(knex) {
  return knex.schema.createTable('covid_clinics', t => {
    t.increments()
    t.string('clinic_name')
    t.string('clinic_address')
    t.string('clinic_latitude', 50)
    t.string('clinic_longitude', 50)
    t.timestamps()
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('covid_clinics')
};
