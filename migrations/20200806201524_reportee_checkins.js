
exports.up = function(knex) {
  return knex.schema.createTable('reportee_checkins', t => {
    t.increments()
    t.integer('reportee_id')
    t.integer('user_id')
    t.integer('created_by')
    t.integer('updated_by')
    t.timestamp('schedule_at')
    t.timestamp('notified_at')
    t.timestamp('checkin_at')
    t.timestamp('created_at').defaultTo(knex.fn.now())
    t.timestamp('updated_at').defaultTo(knex.fn.now())
    t.boolean('checkin_outcome')
    t.string('checkin_outcome_reason')
    t.specificType('checkin_failure_rationale', 'text')
    t.boolean('alert_acknowledged')
    t.boolean('alert_bookmarked')
    t.string('alert_action')
    t.specificType('alert_action_description', 'text')
    t.boolean('follow_up_checkin')
    t.integer('follow_up_parent_id')
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('reportee_checkins')
};
