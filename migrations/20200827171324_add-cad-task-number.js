
exports.up = function(knex) {
  return knex.schema.table('reportee_checkins', t => {
    t.string('cad_task_id', 50)
  })
}; 

exports.down = function(knex) {
  return knex.schema.table('reportee_checkins', t => {
    t.dropColumn('cad_task_id')
  })
};
