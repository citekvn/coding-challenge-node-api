
exports.up = function(knex) {
  return knex.schema.table('reportee_checkins', table => {
    table.specificType('liveness_check_methods', 'text ARRAY')
  })
};

exports.down = function(knex) {
  return knex.schema.table('reportee_checkins', table => {
    table.dropColumn('liveness_check_methods')
  })
};
