## G2G Quarantine Check

### Data Seeds

The script checkinTravellers does the following;

* For the traveller names which are uncommented - removes existing records for those users if they already exist. 
* Approves the travellers with direction
* Scans the travellers at the airport
* Created latitude and longitude for the 'checked_entities'
  
>> trigger build.

>> build trigger.