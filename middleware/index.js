const _ = require('lodash')
const ReporteeModel = require('../models/ReporteeModel');

const getBearerToken = (req) => {
  const headerToken = req.get('Authorization')
  if (!headerToken) {
    return false
  }
  var matches = headerToken.match(/Bearer\s(\S+)/);
  if (!matches) {
    return false
  }
  return matches[1]
}


const adminSession = {
  data: {
    user: {
      id: 1,
      username: 'administrator',
      roles: [{name: 'Admin'}],
      organisation: { id: 1, name: 'WAPOL' }
    }
  }
};

const officerSession = {
  data: {
    user: {
      id: 2,
      username: 'officer',
      roles: [{ name: 'Officer'}],
      organisation: { id: 1, name: 'WAPOL' }
    }
  }
};

const travellerSession = {
  data: {
    user: {
      id: 3,
      username: 'mp4nguyen@gmail.com',
      roles: [{ name: 'Traveller'}]
    }
  }
};



const getSessionFromToken = async (bearerToken) => {
  
  if( bearerToken === 'letAdminIn'){
    return adminSession;
  }else if( bearerToken === 'letOfficerIn'){
    return officerSession;
  }else if( bearerToken === 'letTravellerIn'){
    return travellerSession;
  }else{
    throw new Error("session not found")
  }

}




const bearerTokenMiddleware = async (req, res, next) => {
  
  const bearerToken = getBearerToken(req)

  if (bearerToken) {
    try {
      const session = await getSessionFromToken(bearerToken)     
      
      req.tokenSession = session
      next()
    } catch(e) {
      res.status(401)
      return res.send({ message: 'could not get session from token', error: 'unauthorized', })
    }
  } else {
    res.status(401)
    return res.send({ message: 'no bearer token supplied', error: 'unauthorized', })
  }

}

const reporteeMiddleware = async function (req, res, next) {
  
  let { user }        = req.tokenSession.data;

  if (!user) {
    res.status(401)
    return res.send({
      message: 'Token does not belong to a user session',
      error: 'Unauthorized'
    })
  }

  const reporteeModel = new ReporteeModel();
  let reportee = await reporteeModel.findByUserID({ user_id: user.id})

  req.reportee = reportee

  next();
}


const getRoleNames = (roles) => {
  return roles.map(role => role.name)
}

const validateRoles = (role_names, permitted_roles) => {
  let permitted = false
  if (permitted_roles !== null && Array.isArray(permitted_roles)) {
    role_names.forEach(role => {
      if (permitted_roles.includes(role)) {
        permitted = true;
      }
    })
  }
  return permitted
}

const requiresAuthMiddleware = (permitted_roles) => {

  return function (req, res, next) { 

    let user = _.get(req, 'tokenSession.data.user')

    if (!user) {
      throw new Error('No user session for current request.')
    }

    let role_names = getRoleNames(user.roles);

    let permitted = validateRoles(role_names, permitted_roles)

    if (!permitted) {

      log.error({permitted_roles, role_names, user}, 'Tried to access an resource do not have access to.')
      
      res.status(401)
      
      return res.send({
        status: 'Unauthorized', 
        message: 'You are not authorized to access this resource'
      })

    } else {
      return next();
    }
  }
} 


module.exports = {
  reporteeMiddleware,
  bearerTokenMiddleware,
  requiresAuthMiddleware,
}