const baseCfg = {
    timezone: 'Australia/Perth',
    checkin : {
        maxRadius: 500,
        acceptableRadiusValues: [100,200,500]
    },
    authorisedOrganisations: ['WAPOL'],
    db: {
        dbHosts:[
            {
                name: "g2g_now_reader",
                host: "localhost",
                port: 5432,
                user: "g2gadmin",
                password: process.env.POSTGRES_PASSWORD,                    
            },
        ],        
        host: "localhost",
        port: 5432,
        user: "g2gadmin",
        password: process.env.POSTGRES_PASSWORD,
        defaultTenantDB: "g2g_now",
        tenantDBs: ["g2g_now", "wapol", "taspol"]
    },
    googleAPI: {
      apiKey: process.env.GOOGLE_API_TOKEN
    },
    firebase: {
        dbUrl : "https://g2gnow-staging.firebaseio.com",
        serviceAccount: process.env.HALLO_FIREBASE_SERVICE_ACCOUNT,
    },
    log: {
        name: "checkin-api",
        level: process.env.LOG_LEVEL || "info",
        prettyPrint: false,
    },
    redis: {
        port: 6379,
        host: "localhost",
    },
    schedule: {
        processEvents: process.env.PROCESS_EVENTS,
        defaultCheckinSchedule: "low",
        lateCheckinSpecifier: "5m",
        covidRegisterTestRemindAfter: "30h",
        covidSubmitTestRemindAfter: "42h",
        riskCategories: [ "low", "medium", "high" ],
        schedules: {
            repeating: "1d:1h:10mx4,1d:3h:20mx2",
            test: [1,2,3,4,5,6,7,8,9,10,11,12,13,14].join("d:0h:57mx11,") + "d:19h",
            low: "1d:2h,3d:6h,7d:10h,11d:6h,14d:2h",
            medium: "1d:5h,2d:9h,3d:23h,4d:11h,6d:12h,8d:3h,10d:10h,12d:1h,14d:11h",
            high: "1d:7h,2d:0h,2d:11h,4d:6h,6d:1h,6d:13h,8d:3h,11d:1h,11d:10h,14d:11h",
            // "covid-test": "1d:4h,11d:4h",
            "covid-test": "10d:4h",
            "health-declaration": [1,2,3,4,5,6,7,8,9,10,11,12,13,14].join("d:4h,") + "d:4h",
        },
    },
    aws: {        
        accessKeyId: process.env.AWS_ACCESS_KEY_ID, 
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    },
    sqs: {
        g2gPassQueue: "https://sqs.ap-southeast-2.amazonaws.com/553368204222/dev-g2gpass-queue",
    },
    kms: {
        encryptionAlgorithm: "RSAES_OAEP_SHA_256",
    },
    syncToPass : {
        g2gpassOrgName: 'WAPOL',
        application: {
            url: '/api/internal/v1/traveller/syncapplication',
            method: 'PUT',
        }
    },
    whiteListIPs: '::1,52.62.28.126,3.106.179.6,3.104.245.26,13.210.218.252,13.210.185.69',     
};
// daily on the hour from approx 8am
baseCfg.schedule.schedules.test = [1,2,3,4,5,6,7,8,9,10,11,12,13,14].join("d:7h:57mx12,") + "d:20h";
// console.log("dev schedule : " + baseCfg.schedule.schedules.test);
module.exports = baseCfg;

