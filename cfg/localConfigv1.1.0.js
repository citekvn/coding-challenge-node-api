module.exports = {

  wapol:{
    isConnectToTraveller: true,
    db: {
      dbHosts:[
          {
              name: "dev",
              host: "localhost",
              password: process.env.POSTGRES_PASSWORD,
              port: process.env.POSTGRES_PORT,
              user: process.env.POSTGRES_USER,
          },
          {
            name: "dev-reader",
            host: "localhost",
            password: process.env.POSTGRES_PASSWORD,
            port: process.env.POSTGRES_PORT,
            user: process.env.POSTGRES_USER,
          }          
      ],
      defaultTenantDB: "g2g_now",
      tenantDBs: ["g2g_now", "wapol", "taspol"]
    },
    knex:{
        client: { hostName: 'dev', dbName: 'g2g_now'},
        clientReader: { hostName: 'dev-reader', dbName: 'g2g_now'},
        users: { hostName: 'dev', dbName: 'users'},
        traveller: { hostName: 'dev'},
        travellers: { hostName: 'dev'},
    },

    schedule: {
      processEvents: undefined,
      defaultCheckinSchedule: 'low',
      lateCheckinSpecifier: '5m',
      covidRegisterTestRemindAfter: '10m',
      covidSubmitTestRemindAfter: '10m',
      riskCategories: [ 'test', 'low', 'medium', 'high' ],
      schedules: {
        repeating: '1d:1h:10mx4,1d:3h:20mx2',
        test: '1d:7h:57mx12,2d:7h:57mx12,3d:7h:57mx12,4d:7h:57mx12,5d:7h:57mx12,6d:7h:57mx12,7d:7h:57mx12,8d:7h:57mx12,9d:7h:57mx12,10d:7h:57mx12,11d:7h:57mx12,12d:7h:57mx12,13d:7h:57mx12,14d:20h',
        low: '1d:10h,3d:14h,7d:18h,11d:14h,14d:10h',
        medium: '1d:13h,2d:17h,4d:7h,4d:19h,6d:20h,8d:11h,10d:18h,12d:9h,14d:19h',
        high: '1d:15h,2d:7h,2d:19h,4d:14h,6d:9h,6d:21h,8d:11h,11d:9h,11d:18h,14d:19h',
        'covid-test': '1d:12h,11d:12h',
        'health-declaration': '1d:12h,2d:12h,3d:12h,4d:12h,5d:12h,6d:12h,7d:12h,8d:12h,9d:12h,10d:12h,11d:12h,12d:12h,13d:12h,14d:12h'
      }
    },
      
    firebase: {
      serviceAccount: "ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAiZzJnbm93LXN0YWdpbmciLAogICJwcml2YXRlX2tleV9pZCI6ICJiZTBiZDIwNGM1ZjcyMDliM2I0ZjIyNTg0YWM3NGNiODc4ZDg4YmY2IiwKICAicHJpdmF0ZV9rZXkiOiAiLS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tXG5NSUlFdmdJQkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0JLZ3dnZ1NrQWdFQUFvSUJBUUNxNkw4SzZXQjhYWDB1XG5nMXRLZWVKc3QvNWJna2pQc2d3MHQzaUMzeURna3lSZm9qTWJsb0RTVEl4aDFLQm04UnNqNmZqWGErVkwxY2E1XG4zNXYxRURUblh2YWdJR1N3ZWJlb2FNSjRJQmhNWGM0bFhBM2RuaU85MUpjQXZudVlCakZWc2x6YVJETkRYSDM1XG5nRStzSXJib0ZkTkJKeEtvcjRhSjZhY1JHSXFHVUN6OXZmMXNHTTFoMWZ4YXRMZHZBK1phbjFqc2xoSTJqK0VpXG5jR0dVUGloRWpkNHFSSGViR000SXdvbGdodlZ1WThXMGp4NzdGdGZBWTVhWUQrU1FQbXlDYnpjN1NPK2pXZGdMXG5sL3gvODdrdlRpUGJ4dHN6R2puUi95c0QxbHE0a3BzdnVtU2lFTVVoVEh4VjVKdURxNDRQNTlSY3NTY0lSVlZLXG5hekhYL3pCTkFnTUJBQUVDZ2dFQVUySkx0d0NtM1BabDJWZ29KWjF5elpVMVNhTTJiL0xQeHJhTzJ0Y08vTko5XG5TQlZrTnYwR2E5UUNGOGI3M1UyYjdyazcrYXBBREhCSHpTN2VNY0hPOWVWY2tVSk1yZGNzdGtNcVFoNXlqR3VsXG41STlWaW44WnRBZXhCcUZvdm9LaTlrc1h4enZJQVFEeXkxU0JTcHdEUGhkTDR2a3p2czRvWjBmeWhVSkNiNFdJXG5DdDh1SDlBanRBalRqZVRqQitwdHUrSExWNUkweTRzWmVXbXBCb3RRKzVVNWNJTDdOQnlzOTZaeUl0alhkayt6XG5xVW55blVhVmdDTklvRm1XOVBCYjQ3QkQwRmY0QWpsenI5V0hMbXlCTnNnNHJLcDJScWxkbDhTb2xDZmY1Q0xIXG5zNktCdDU1LzZHV05pWGxSRlQvbFlrUnFLSWRYWDVOUi95WXl5ekVrTndLQmdRRHFSSS83Vm9uTDQ1ZFRzck1TXG5ySmszcGsyOW1ndS9UdkdLZ200VmIrUHNiSHlUMlcyZ2FQSFkxU1l3YzVUMzl1SnlkclUrc29Vak0rRlhWa09WXG5GbHN1SFk1RHFvM2VPS1ZOQjJHbGlHWnhjUjBYYnBMZEV2c3Z1dXZLVmNDSXBwbHkrVno1cTJxZVE5UWtLSk1aXG5XNGZhdWxqVjhlQ0R1VWhOakkzbWV3VmtId0tCZ1FDNnc0ZTZTSFlJcDBrRXN5bnRwS0loSWhEbHVnNzFaMzAvXG5kRmhZczg3Y3pBMWtHVE1UamlWOTlMcTBqRmsxdXYxWkpITXc2UEdBM3JJczJHUngrcmxMc1RqcDIxUHljVTJGXG5pck9qc2srcmlEdXRaMXlCWTVzYnRndGJRdDA2SkJ4QUZvdS9lRVZGT040RGxOYUt2MEtTY2xycHRJZWN6ZFQ1XG5SME90S243K0V3S0JnUUNKSU4wUENGYmNyRG8yV2hJTVNrL2RqVkZRUE0zSGpCUkRPWm5xbm5aU1BDYW4vTXY0XG42N1UxY1g5TE9Uem44UElZZ25nUlhUb2xvd2F5c3VJWWNOK3FuTTVidVlnVm5oN1VlZytrYjdWWGpoaXpVSnhUXG5IL3M4c3pPMlduKzYvWm93L05wNW91ZGtBTnc2MWkxVTJJa3JkWXp1bndrRzBmd0t1TTJQTGsrM2pRS0JnUUNnXG5ZdlIzZEd6eEI0QThhZ1JTMGZSdDBjeURER0oyT3hhalhLM1dGeXhLRDI1Y2pRdUErcXlWMjRwUExNd2J5V1p5XG5uRXZVTCtmZURPRWtnL3pXcVVOaGptTmE2WURSRHA1TlZZenB3bUtLNDJha2hzUTVWYW5KaFcrQlF3MnJOWUdSXG5SQXRsTmNyL0pGUDg3Vi9GTFhUbFFSRno3QlllQWVHbzVadG41YnZEUXdLQmdGRjdyWU9JK1BFTys4bmoxbWdLXG44ZlZKU2s4cXdUNnQvVVRTbzVDTEVpU21vbUVVZjFjemVoSGkvSEgvR3RTcGlKQnFmOWtqb1FDTThxQnBrUVVGXG5TaEcyQW5sZkxZdXdIZWtwZjhqNW1lVUlEU0Zoc1liem9iaUNSOUsrVlJhTG9wZzQrYzhDTjJ4SWlLVDRGczVvXG5CZjdmMU9wMVlRQVdKd0FhU0plSUQxelVcbi0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS1cbiIsCiAgImNsaWVudF9lbWFpbCI6ICJmaXJlYmFzZS1hZG1pbnNkay0yY3lpdEBnMmdub3ctc3RhZ2luZy5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsCiAgImNsaWVudF9pZCI6ICIxMDkyNzYyMTIxNTkzOTY0NTkwMDQiLAogICJhdXRoX3VyaSI6ICJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20vby9vYXV0aDIvYXV0aCIsCiAgInRva2VuX3VyaSI6ICJodHRwczovL29hdXRoMi5nb29nbGVhcGlzLmNvbS90b2tlbiIsCiAgImF1dGhfcHJvdmlkZXJfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9vYXV0aDIvdjEvY2VydHMiLAogICJjbGllbnRfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9yb2JvdC92MS9tZXRhZGF0YS94NTA5L2ZpcmViYXNlLWFkbWluc2RrLTJjeWl0JTQwZzJnbm93LXN0YWdpbmcuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iCn0K"
    },
    log: {
        prettyPrint: true,
        level: 'debug'
    },
    redis: {
      host: 'localhost',
      port: 6379,
    },
    syncToPass : {
      baseUrl: 'https://qcode.api.wapf.genvis.co',
    },  
    oauthServer: process.env.OAUTH_SERVER,//"http://localhost:3030",
    s3: {
        phocus: '<S3 link>'
    },  
  },

  sah: {

    isConnectToTraveller: false,
    db: {
      dbHosts:[
        {
          name: "sah",
          host: process.env.SAH_HOST_POSTGRES_HOST,
          password: process.env.SAH_HOST_POSTGRES_PASSWORD,
          port: process.env.SAH_HOST_POSTGRES_PORT,
          user: process.env.SAH_HOST_POSTGRES_USER,
        },
        {
          name: "users",
          host: process.env.USERS_HOST_POSTGRES_HOST,
          password: process.env.USERS_HOST_POSTGRES_PASSWORD,
          port: process.env.USERS_HOST_POSTGRES_PORT,
          user: process.env.USERS_HOST_POSTGRES_USER,
        }          
      ],
      defaultTenantDB: "g2g_now",
      tenantDBs: ["g2g_now", "wapol", "taspol"]
    },
    knex:{
        client: { hostName: 'sah', dbName: 'g2g_now'},
        users: { hostName: 'users', dbName: 'users'},
        traveller: { hostName: 'users'},
        travellers: { hostName: 'users'},
    },
    
    
    firebase: {
      serviceAccount: "ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAiZzJnbm93LXN0YWdpbmciLAogICJwcml2YXRlX2tleV9pZCI6ICJiZTBiZDIwNGM1ZjcyMDliM2I0ZjIyNTg0YWM3NGNiODc4ZDg4YmY2IiwKICAicHJpdmF0ZV9rZXkiOiAiLS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tXG5NSUlFdmdJQkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0JLZ3dnZ1NrQWdFQUFvSUJBUUNxNkw4SzZXQjhYWDB1XG5nMXRLZWVKc3QvNWJna2pQc2d3MHQzaUMzeURna3lSZm9qTWJsb0RTVEl4aDFLQm04UnNqNmZqWGErVkwxY2E1XG4zNXYxRURUblh2YWdJR1N3ZWJlb2FNSjRJQmhNWGM0bFhBM2RuaU85MUpjQXZudVlCakZWc2x6YVJETkRYSDM1XG5nRStzSXJib0ZkTkJKeEtvcjRhSjZhY1JHSXFHVUN6OXZmMXNHTTFoMWZ4YXRMZHZBK1phbjFqc2xoSTJqK0VpXG5jR0dVUGloRWpkNHFSSGViR000SXdvbGdodlZ1WThXMGp4NzdGdGZBWTVhWUQrU1FQbXlDYnpjN1NPK2pXZGdMXG5sL3gvODdrdlRpUGJ4dHN6R2puUi95c0QxbHE0a3BzdnVtU2lFTVVoVEh4VjVKdURxNDRQNTlSY3NTY0lSVlZLXG5hekhYL3pCTkFnTUJBQUVDZ2dFQVUySkx0d0NtM1BabDJWZ29KWjF5elpVMVNhTTJiL0xQeHJhTzJ0Y08vTko5XG5TQlZrTnYwR2E5UUNGOGI3M1UyYjdyazcrYXBBREhCSHpTN2VNY0hPOWVWY2tVSk1yZGNzdGtNcVFoNXlqR3VsXG41STlWaW44WnRBZXhCcUZvdm9LaTlrc1h4enZJQVFEeXkxU0JTcHdEUGhkTDR2a3p2czRvWjBmeWhVSkNiNFdJXG5DdDh1SDlBanRBalRqZVRqQitwdHUrSExWNUkweTRzWmVXbXBCb3RRKzVVNWNJTDdOQnlzOTZaeUl0alhkayt6XG5xVW55blVhVmdDTklvRm1XOVBCYjQ3QkQwRmY0QWpsenI5V0hMbXlCTnNnNHJLcDJScWxkbDhTb2xDZmY1Q0xIXG5zNktCdDU1LzZHV05pWGxSRlQvbFlrUnFLSWRYWDVOUi95WXl5ekVrTndLQmdRRHFSSS83Vm9uTDQ1ZFRzck1TXG5ySmszcGsyOW1ndS9UdkdLZ200VmIrUHNiSHlUMlcyZ2FQSFkxU1l3YzVUMzl1SnlkclUrc29Vak0rRlhWa09WXG5GbHN1SFk1RHFvM2VPS1ZOQjJHbGlHWnhjUjBYYnBMZEV2c3Z1dXZLVmNDSXBwbHkrVno1cTJxZVE5UWtLSk1aXG5XNGZhdWxqVjhlQ0R1VWhOakkzbWV3VmtId0tCZ1FDNnc0ZTZTSFlJcDBrRXN5bnRwS0loSWhEbHVnNzFaMzAvXG5kRmhZczg3Y3pBMWtHVE1UamlWOTlMcTBqRmsxdXYxWkpITXc2UEdBM3JJczJHUngrcmxMc1RqcDIxUHljVTJGXG5pck9qc2srcmlEdXRaMXlCWTVzYnRndGJRdDA2SkJ4QUZvdS9lRVZGT040RGxOYUt2MEtTY2xycHRJZWN6ZFQ1XG5SME90S243K0V3S0JnUUNKSU4wUENGYmNyRG8yV2hJTVNrL2RqVkZRUE0zSGpCUkRPWm5xbm5aU1BDYW4vTXY0XG42N1UxY1g5TE9Uem44UElZZ25nUlhUb2xvd2F5c3VJWWNOK3FuTTVidVlnVm5oN1VlZytrYjdWWGpoaXpVSnhUXG5IL3M4c3pPMlduKzYvWm93L05wNW91ZGtBTnc2MWkxVTJJa3JkWXp1bndrRzBmd0t1TTJQTGsrM2pRS0JnUUNnXG5ZdlIzZEd6eEI0QThhZ1JTMGZSdDBjeURER0oyT3hhalhLM1dGeXhLRDI1Y2pRdUErcXlWMjRwUExNd2J5V1p5XG5uRXZVTCtmZURPRWtnL3pXcVVOaGptTmE2WURSRHA1TlZZenB3bUtLNDJha2hzUTVWYW5KaFcrQlF3MnJOWUdSXG5SQXRsTmNyL0pGUDg3Vi9GTFhUbFFSRno3QlllQWVHbzVadG41YnZEUXdLQmdGRjdyWU9JK1BFTys4bmoxbWdLXG44ZlZKU2s4cXdUNnQvVVRTbzVDTEVpU21vbUVVZjFjemVoSGkvSEgvR3RTcGlKQnFmOWtqb1FDTThxQnBrUVVGXG5TaEcyQW5sZkxZdXdIZWtwZjhqNW1lVUlEU0Zoc1liem9iaUNSOUsrVlJhTG9wZzQrYzhDTjJ4SWlLVDRGczVvXG5CZjdmMU9wMVlRQVdKd0FhU0plSUQxelVcbi0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS1cbiIsCiAgImNsaWVudF9lbWFpbCI6ICJmaXJlYmFzZS1hZG1pbnNkay0yY3lpdEBnMmdub3ctc3RhZ2luZy5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsCiAgImNsaWVudF9pZCI6ICIxMDkyNzYyMTIxNTkzOTY0NTkwMDQiLAogICJhdXRoX3VyaSI6ICJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20vby9vYXV0aDIvYXV0aCIsCiAgInRva2VuX3VyaSI6ICJodHRwczovL29hdXRoMi5nb29nbGVhcGlzLmNvbS90b2tlbiIsCiAgImF1dGhfcHJvdmlkZXJfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9vYXV0aDIvdjEvY2VydHMiLAogICJjbGllbnRfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9yb2JvdC92MS9tZXRhZGF0YS94NTA5L2ZpcmViYXNlLWFkbWluc2RrLTJjeWl0JTQwZzJnbm93LXN0YWdpbmcuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iCn0K"
    },
    log: {
        prettyPrint: true,
        level: 'debug'
    },
    redis: {
      host: 'localhost',
      port: 6379,
    },
    schedule: {
      processEvents: false
    } ,
    syncToPass : {
      baseUrl: 'https://qcode.api.wapf.genvis.co',
    },  
    oauthServer: "http://localhost:3030",
    s3: {
        phocus: '<S3 link>'
    },  
  },
  

  ntg: {

    isConnectToTraveller: false,

    userApi: {
      baseUrl: "http://localhost:3030",
      serviceUsername: 'ntg.g2g-now.service@genvis.co',
      servicePassword: process.env.SERVICE_ACCOUNT_PASSWORD,
      serviceClientId: 'checkin-api',
      serviceClientSecret: 'sup3rs3cret'
    },            

    db: {
      dbHosts:[
        {
          name: "ntg",
          host: process.env.TNG_HOST_POSTGRES_HOST,
          password: process.env.TNG_HOST_POSTGRES_PASSWORD,
          port: process.env.TNG_HOST_POSTGRES_PORT,
          user: process.env.TNG_HOST_POSTGRES_USER,
        },
        {
          name: "ntg-reader",
          host: process.env.TNG_HOST_POSTGRES_HOST,
          password: process.env.TNG_HOST_POSTGRES_PASSWORD,
          port: process.env.NTG_HOST_POSTGRES_READER_PORT,
          user: process.env.TNG_HOST_POSTGRES_USER,
        },        
        {
          name: "users",
          host: process.env.USERS_HOST_POSTGRES_HOST,
          password: process.env.USERS_HOST_POSTGRES_PASSWORD,
          port: process.env.USERS_HOST_POSTGRES_PORT,
          user: process.env.USERS_HOST_POSTGRES_USER,
        }          
      ],
      defaultTenantDB: "g2g_now",
      tenantDBs: ["g2g_now", "wapol", "taspol"]
    },

    knex:{
      client: { hostName: 'ntg', dbName: 'g2g_now'},
      clientReader: { hostName: 'ntg-reader', dbName: 'g2g_now'},
      users: { hostName: 'users', dbName: 'users'},
      traveller: { hostName: 'users'},
      travellers: { hostName: 'users'},
    },
        
    firebase: {
      serviceAccount: "ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAiZzJnbm93LXN0YWdpbmciLAogICJwcml2YXRlX2tleV9pZCI6ICJiZTBiZDIwNGM1ZjcyMDliM2I0ZjIyNTg0YWM3NGNiODc4ZDg4YmY2IiwKICAicHJpdmF0ZV9rZXkiOiAiLS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tXG5NSUlFdmdJQkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0JLZ3dnZ1NrQWdFQUFvSUJBUUNxNkw4SzZXQjhYWDB1XG5nMXRLZWVKc3QvNWJna2pQc2d3MHQzaUMzeURna3lSZm9qTWJsb0RTVEl4aDFLQm04UnNqNmZqWGErVkwxY2E1XG4zNXYxRURUblh2YWdJR1N3ZWJlb2FNSjRJQmhNWGM0bFhBM2RuaU85MUpjQXZudVlCakZWc2x6YVJETkRYSDM1XG5nRStzSXJib0ZkTkJKeEtvcjRhSjZhY1JHSXFHVUN6OXZmMXNHTTFoMWZ4YXRMZHZBK1phbjFqc2xoSTJqK0VpXG5jR0dVUGloRWpkNHFSSGViR000SXdvbGdodlZ1WThXMGp4NzdGdGZBWTVhWUQrU1FQbXlDYnpjN1NPK2pXZGdMXG5sL3gvODdrdlRpUGJ4dHN6R2puUi95c0QxbHE0a3BzdnVtU2lFTVVoVEh4VjVKdURxNDRQNTlSY3NTY0lSVlZLXG5hekhYL3pCTkFnTUJBQUVDZ2dFQVUySkx0d0NtM1BabDJWZ29KWjF5elpVMVNhTTJiL0xQeHJhTzJ0Y08vTko5XG5TQlZrTnYwR2E5UUNGOGI3M1UyYjdyazcrYXBBREhCSHpTN2VNY0hPOWVWY2tVSk1yZGNzdGtNcVFoNXlqR3VsXG41STlWaW44WnRBZXhCcUZvdm9LaTlrc1h4enZJQVFEeXkxU0JTcHdEUGhkTDR2a3p2czRvWjBmeWhVSkNiNFdJXG5DdDh1SDlBanRBalRqZVRqQitwdHUrSExWNUkweTRzWmVXbXBCb3RRKzVVNWNJTDdOQnlzOTZaeUl0alhkayt6XG5xVW55blVhVmdDTklvRm1XOVBCYjQ3QkQwRmY0QWpsenI5V0hMbXlCTnNnNHJLcDJScWxkbDhTb2xDZmY1Q0xIXG5zNktCdDU1LzZHV05pWGxSRlQvbFlrUnFLSWRYWDVOUi95WXl5ekVrTndLQmdRRHFSSS83Vm9uTDQ1ZFRzck1TXG5ySmszcGsyOW1ndS9UdkdLZ200VmIrUHNiSHlUMlcyZ2FQSFkxU1l3YzVUMzl1SnlkclUrc29Vak0rRlhWa09WXG5GbHN1SFk1RHFvM2VPS1ZOQjJHbGlHWnhjUjBYYnBMZEV2c3Z1dXZLVmNDSXBwbHkrVno1cTJxZVE5UWtLSk1aXG5XNGZhdWxqVjhlQ0R1VWhOakkzbWV3VmtId0tCZ1FDNnc0ZTZTSFlJcDBrRXN5bnRwS0loSWhEbHVnNzFaMzAvXG5kRmhZczg3Y3pBMWtHVE1UamlWOTlMcTBqRmsxdXYxWkpITXc2UEdBM3JJczJHUngrcmxMc1RqcDIxUHljVTJGXG5pck9qc2srcmlEdXRaMXlCWTVzYnRndGJRdDA2SkJ4QUZvdS9lRVZGT040RGxOYUt2MEtTY2xycHRJZWN6ZFQ1XG5SME90S243K0V3S0JnUUNKSU4wUENGYmNyRG8yV2hJTVNrL2RqVkZRUE0zSGpCUkRPWm5xbm5aU1BDYW4vTXY0XG42N1UxY1g5TE9Uem44UElZZ25nUlhUb2xvd2F5c3VJWWNOK3FuTTVidVlnVm5oN1VlZytrYjdWWGpoaXpVSnhUXG5IL3M4c3pPMlduKzYvWm93L05wNW91ZGtBTnc2MWkxVTJJa3JkWXp1bndrRzBmd0t1TTJQTGsrM2pRS0JnUUNnXG5ZdlIzZEd6eEI0QThhZ1JTMGZSdDBjeURER0oyT3hhalhLM1dGeXhLRDI1Y2pRdUErcXlWMjRwUExNd2J5V1p5XG5uRXZVTCtmZURPRWtnL3pXcVVOaGptTmE2WURSRHA1TlZZenB3bUtLNDJha2hzUTVWYW5KaFcrQlF3MnJOWUdSXG5SQXRsTmNyL0pGUDg3Vi9GTFhUbFFSRno3QlllQWVHbzVadG41YnZEUXdLQmdGRjdyWU9JK1BFTys4bmoxbWdLXG44ZlZKU2s4cXdUNnQvVVRTbzVDTEVpU21vbUVVZjFjemVoSGkvSEgvR3RTcGlKQnFmOWtqb1FDTThxQnBrUVVGXG5TaEcyQW5sZkxZdXdIZWtwZjhqNW1lVUlEU0Zoc1liem9iaUNSOUsrVlJhTG9wZzQrYzhDTjJ4SWlLVDRGczVvXG5CZjdmMU9wMVlRQVdKd0FhU0plSUQxelVcbi0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS1cbiIsCiAgImNsaWVudF9lbWFpbCI6ICJmaXJlYmFzZS1hZG1pbnNkay0yY3lpdEBnMmdub3ctc3RhZ2luZy5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsCiAgImNsaWVudF9pZCI6ICIxMDkyNzYyMTIxNTkzOTY0NTkwMDQiLAogICJhdXRoX3VyaSI6ICJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20vby9vYXV0aDIvYXV0aCIsCiAgInRva2VuX3VyaSI6ICJodHRwczovL29hdXRoMi5nb29nbGVhcGlzLmNvbS90b2tlbiIsCiAgImF1dGhfcHJvdmlkZXJfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9vYXV0aDIvdjEvY2VydHMiLAogICJjbGllbnRfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9yb2JvdC92MS9tZXRhZGF0YS94NTA5L2ZpcmViYXNlLWFkbWluc2RrLTJjeWl0JTQwZzJnbm93LXN0YWdpbmcuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iCn0K"
    },
    log: {
        prettyPrint: true,
        level: 'debug'
    },
    redis: {
      host: 'localhost',
      port: 6379,
    },
    schedule: {
      processEvents: false
    } ,
    syncToPass : {
      baseUrl: 'https://qcode.api.wapf.genvis.co',
    },  
    oauthServer: "http://localhost:3030",
    s3: {
        phocus: '<S3 link>'
    },  
  },


}