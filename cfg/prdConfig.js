module.exports = {
  checkin : {
    maxRadius: 200
  },
  db: {
    dbHosts:[
      {
          name: "g2g_now_reader",
          host: 'prd-g2g-db-cluster.cluster-ro-cxl1by9steoq.ap-southeast-2.rds.amazonaws.com'
      },
    ],                  
    host: "prd-g2g-db-cluster.cluster-cxl1by9steoq.ap-southeast-2.rds.amazonaws.com"
  },
  firebase: {
    dbUrl : "https://g2gnow-54da3.firebaseio.com",
  },
  schedule: {
    riskCategories: [ "low", "medium", "high" ],
  },
  oauthServer: "https://id.api.wapf.genvis.co",
  sqs: {
    g2gPassQueue: "https://sqs.ap-southeast-2.amazonaws.com/637237400088/prod-g2gpass-queue",
  },   
  kms: {
    shouldEncrypt: false,
    keyId: "arn:aws:kms:ap-southeast-2:637237400088:key/0fc91889-3d11-4fe8-a941-95cfaa4502f0"
  } ,
  redis: {
    host: "g2g-production.b5a0a2.ng.0001.apse2.cache.amazonaws.com",
    tls: {},
    password: process.env.REDIS_PASSWORD
  },
  whiteListIPs: '::1,13.236.196.183,3.106.63.63,3.24.121.174',

};