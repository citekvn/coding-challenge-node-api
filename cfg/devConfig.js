module.exports = {
    redis: {
        host: "g2g-redis-dev.i1yvzu.ng.0001.apse2.cache.amazonaws.com",
    },
    schedule : {
        covidRegisterTestRemindAfter: "10m",
        covidSubmitTestRemindAfter: "10m",
        riskCategories: [ "test", "low", "medium", "high" ],
        schedules: {
            repeating: "1d:1h:10mx4,1d:3h:20mx2",
            // dev: "0d:10h,0d:11h,0d:12h,0d:14h,0d:15h,0d:16h,1d:10h,1d:11h,1d:12h,1d:13h,1d:14h,1d:15h,1d:16h,1d:17h,2d:10h,2d:11h,2d:12h",
            low: "1d:10h,3d:14h,7d:18h,11d:14h,14d:10h",
            medium: "1d:13h,2d:17h,4d:7h,4d:19h,6d:20h,8d:11h,10d:18h,12d:9h,14d:19h",
            high: "1d:15h,2d:7h,2d:19h,4d:14h,6d:9h,6d:21h,8d:11h,11d:9h,11d:18h,14d:19h",
            "covid-test": "1d:12h,11d:12h",
          "health-declaration": "1d:12h,2d:12h,3d:12h,4d:12h,5d:12h,6d:12h,7d:12h,8d:12h,9d:12h,10d:12h,11d:12h,12d:12h,13d:12h,14d:12h"
      }
    },
    kms: {
      shouldEncrypt: true,
      keyId: 'arn:aws:kms:ap-southeast-2:553368204222:key/b95d1535-33df-4efb-893e-be134b7ddf4a'
    },
    db: {
      dbHosts:[
        {
            name: "g2g_now_reader",
            host: 'dev-g2g-db-instance-1-cluster-cluster.cluster-ro-ccbuvz4gnbub.ap-southeast-2.rds.amazonaws.com'
        },
      ],              
      host: 'dev-g2g-db-instance-1-cluster.ccbuvz4gnbub.ap-southeast-2.rds.amazonaws.com'
    },
    sqs: {
      g2gPassQueue: "https://sqs.ap-southeast-2.amazonaws.com/553368204222/dev-g2gpass-queue",
    },    
    oauthServer: "https://id.api.wapfdev.genvis.co",
};