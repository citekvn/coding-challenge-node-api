// coming ..... configs

const base = require("./baseConfig");
const dev = require("./devConfig");
let local = {};
let test = {};
const prd = require("./prdConfig");
const _ = require("lodash");

let config = null;

try {
    local = require("./localConfigMaster");
} catch (err) {
    console.log('failed to load localConfig', err)
}

try {
  test = require('./testConfig');
} catch (err) {
    console.log('failed to load testConfig', err)
}

function load (envName) {
    const env = envName || process.env.GENVIS_ENV || "dev";
     console.log('loading config > ', env)

    if (env === "local") {
        config = _.merge(base, local);
    }
    else if (env === "dev") {
        config = _.merge(base, dev);
    }
    else if (env === "prd") {
        config = _.merge(base, prd);
    }
    else if (env === "test") {
        config = _.merge(base, test)
    }
    else {
        throw Error("unsupported env " + env);
    }
    return config;
}

function getConfig() {
    return config;
}

if (config == null) {
    config = load();
}

module.exports = config;
module.exports.getConfig = getConfig;
module.exports.load = load;
