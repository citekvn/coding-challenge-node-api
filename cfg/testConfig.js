module.exports = {
  db: {
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT,
    user: process.env.POSTGRES_USER,
    travellerClient: 'wapol'
  },
  log: {
      prettyPrint: true
  },
  redis: {
    // host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASSWORD
  },
  schedule : {
    covidRegisterTestRemindAfter: "10m",
    covidSubmitTestRemindAfter: "10m",
    riskCategories: ["test", "low", "medium", "high"],
    schedules: {
      repeating: "1d:1h:10mx4,1d:3h:20mx2",
      // dev: "0d:10h,0d:11h,0d:12h,0d:14h,0d:15h,0d:16h,1d:10h,1d:11h,1d:12h,1d:13h,1d:14h,1d:15h,1d:16h,1d:17h,2d:10h,2d:11h,2d:12h",
      low: "1d:10h,3d:14h,7d:18h,11d:14h,14d:10h",
      medium: "1d:13h,2d:17h,4d:7h,4d:19h,6d:20h,8d:11h,10d:18h,12d:9h,14d:19h",
      high: "1d:15h,2d:7h,2d:19h,4d:14h,6d:9h,6d:21h,8d:11h,11d:9h,11d:18h,14d:19h",
      "covid-test": "1d:12h,11d:12h",
      "health-declaration": "1d:12h,2d:12h,3d:12h,4d:12h,5d:12h,6d:12h,7d:12h,8d:12h,9d:12h,10d:12h,11d:12h,12d:12h,13d:12h,14d:12h"
    }
  }
}