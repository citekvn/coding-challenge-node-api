const config = require('./cfg/config.js');
const Log = require("@genvis/genvis-log").GenVisLog;
const log = Log.init(config.log);

const knexPool = require("@genvis/genvis-knex");
knexPool.init(config.db);

const express = require('express')
var cors = require('cors')
const routes = require('./routes')
const app = express()
const server = require('http').createServer(app);

app.use(express.json({limit: '500mb'}))
app.use(cors())

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.setHeader('Access-Control-Allow-Credentials', true)

    if ('OPTIONS' === req.method) {
        res.send(200);
    }
    else {
        next();
    }
});

routes(app)

const PORT = process.env.PORT || 5000;

module.exports = { server, app, log, PORT };