FROM node:10

ARG NPM_TOKEN
# Use the passed argument as an environment variable
ENV NPM_TOKEN=$NPM_TOKEN

WORKDIR /usr/app
# Copy all the files from your host current directory (except .dockerignore file matches)
COPY . .

# Install production dependencies
RUN npm i --only=production
RUN npm dedup

# Remove README and other non executable files in node_modules
RUN npx nm-prune --force --prune-license
# Remove all dot files (usually ignore file, rc files, dev files, etc)
RUN find . -maxdepth 1 -type f -name ".*" -exec rm "{}" \;
# Remove few other files manually
RUN rm bitbucket-pipelines.yml || echo could not remove bitbucket-pipelines.yml
RUN rm -rf ./test/ || echo could not remove ./test/

COPY . /usr/app

RUN npx nm-prune --force --prune-license
# Remove all dot files (usually ignore file, rc files, dev files, etc)
RUN find . -maxdepth 1 -type f -name ".*" -exec rm "{}" \;
# Remove few other files manually
RUN rm bitbucket-pipelines.yml || echo could not remove bitbucket-pipelines.yml
RUN rm -rf ./test/ || echo could not remove ./test/

EXPOSE 8080

CMD ["node", "index.js"]